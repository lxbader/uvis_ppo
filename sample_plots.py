# plot HST/UVIS files
import path_register
pr = path_register.PathRegister()
gitpath = pr.gitpath
datapath = pr.datapath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cubehelix
from datetime import datetime
import get_image
import glob
import matplotlib.colors as colors
import matplotlib.gridspec as gridspec
import matplotlib.patches as ptch
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
import numpy as np
import time_conversions as tc
import uvisdb

myblue='royalblue'
myred='crimson'

udb = uvisdb.UVISDB()
imglist = udb.currentDF

cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, minLight=0.15, gamma=1.5)

samplepath = '{}/uvis_ppo/data_sample'.format(gitpath)
samplefile = glob.glob('{}/*.fits'.format(samplepath))[0]
fname = samplefile.strip('.fits').split('/')[-1].split('\\')[-1]

sampleind = 0
for iii in range(len(imglist)):
    if fname in imglist['FILEPATH'].loc[iii]:
        sampleind = iii

KR_MIN=0.5
KR_MAX=30
scale = 'log'

image, angles, hemsph = get_image.getRedUVIS(samplefile, minangle=0)
image[np.where(image<=0)] = 0.00001
theta = np.linspace(0,2*np.pi,num=np.shape(image)[0]+1,endpoint=True)
r = np.linspace(0,30,num=np.shape(image)[1]+1,endpoint=True)

etstart = imglist['ET_START'].loc[sampleind]
etstop = imglist['ET_STOP'].loc[sampleind]
hemsph = imglist['HEMISPHERE'].loc[sampleind]
exp = imglist['EXP'].loc[sampleind]
ppo_n = imglist['PPO_PHASE_N'].loc[sampleind]
ppo_s = imglist['PPO_PHASE_S'].loc[sampleind]

pystart = tc.et2datetime(etstart)
timestr = datetime.strftime(pystart, '%Y-%j %H:%M:%S')
#%%
# make figure
fig = plt.figure()
fig.set_size_inches(13,6)
gs = gridspec.GridSpec(1, 3, width_ratios=(1, 1, 0.06), wspace=0.3)

ax1 = plt.subplot(gs[0,0], projection='polar')

# plot colormap and colorbar
quad = ax1.pcolormesh(theta, r, image.T, cmap=cmap_UV)
quad.set_clim(0, KR_MAX)
quad.set_norm(colors.LogNorm(KR_MIN,KR_MAX))
ax1.set_facecolor('gray')

# PPO phase angle
if np.isfinite(ppo_s):
    ax1.plot([ppo_s,ppo_s], [0,30], color=myblue, lw=4)
    ax1.text(ppo_s+0.05, 35, 'PPO S\n$\Psi_\mathrm{S}=0^\mathrm{o}$', color=myblue,
            fontsize=12, horizontalalignment='center', verticalalignment='center')

# draw grids and annotate lines
ppo_act = (ppo_s) % (2*np.pi)
ppo_lines = [ppo_act, ppo_act-np.pi/4, ppo_act-np.pi/2]
texts = ['','$45^\mathrm{o}$','$90^\mathrm{o}$']
for iii in range(1,len(ppo_lines)):
    ppol = ppo_lines[iii]
    ax1.plot([ppol,ppol], [0,30], c=myblue, lw=2, ls='--')
    ax1.text(ppol, 33, texts[iii], color=myblue,
            fontsize=12, horizontalalignment='center', verticalalignment='center')
    ax1.fill_between(np.linspace(ppo_lines[iii], ppo_lines[iii-1],num=100),0,30,
                    color='white', alpha=0.3*iii)
xticklines = np.linspace(0,2*np.pi,num=8,endpoint=False)
tmp = np.where(np.logical_and(xticklines<np.max(ppo_lines), xticklines>np.min(ppo_lines)))[0]
all_binlines = np.sort(np.append(ppo_lines,xticklines[tmp]))
texts = ['LT 9-12\n$\Psi_\mathrm{S}\,\,45-90^\mathrm{o}$',
         'LT 12-15\n$\Psi_\mathrm{S}\,\,45-90^\mathrm{o}$',
         'LT 12-15\n$\Psi_\mathrm{S}\,\,0-45^\mathrm{o}$',
         'LT 15-18\n$\Psi_\mathrm{S}\,\,0-45^\mathrm{o}$']
for iii in range(len(all_binlines)-1):
    ccc = '0.95' if iii%2 else '0.2'
    ax1.fill_between(np.linspace(all_binlines[iii], all_binlines[iii+1],num=100),28,30,
                    color=ccc, alpha=1)
    diff = np.radians(1)
    ax1.fill_between(np.linspace(all_binlines[iii], all_binlines[iii]+diff,num=10),0,30,
                    color=ccc, alpha=1)
    ax1.fill_between(np.linspace(all_binlines[iii+1]-1.5*diff, all_binlines[iii+1],num=10),0,30,
                    color=ccc, alpha=1)
#    ax1.plot([all_binlines[iii]+diff,all_binlines[iii]+diff],
#             [0,30], 
#             color=ccc, ls='--', zorder=1)
#    ax1.plot([all_binlines[iii+1]-diff,all_binlines[iii+1]-diff],
#             [0,30], 
#             color=ccc, ls='--', zorder=1)
    ax1.annotate(texts[iii],
                xy=(np.mean([all_binlines[iii], all_binlines[iii+1]]), 30), 
                xycoords='data',
                xytext=(np.mean([all_binlines[iii], all_binlines[iii+1]]), 40),
                textcoords='data',
                color='k',
                arrowprops=dict(arrowstyle='simple', facecolor='k', edgecolor='k'),
                horizontalalignment='center', verticalalignment='center',
                fontsize=11
                )
                    
# general figure setup 
ax1.set_title('{}\n{}, {:.1f} min'.format(timestr, hemsph, exp/60), fontsize=13, y=1.08)
ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
ticklabels = ['00','06','12','18']
for iii in range(len(ticklabels)):
    txt = ax1.text(ticks[iii], 27, ticklabels[iii], color='w', fontsize=16, fontweight='bold',
                  ha='center',va='center')
    txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
ax1.set_xticks(ticks)
ax1.set_xticklabels([])
ax1.set_yticks([10,20,30])
ax1.set_yticklabels([])
ax1.grid('on', color='0.8', linewidth=1)
ax1.set_theta_zero_location("N")
ax1.set_rmax(30)
ax1.set_rmin(0)

txt = ax1.text(0, 1, '(a)',
              transform=ax1.transAxes, ha='center', va='center',
              color='k', fontweight='bold', fontsize=20)
txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])

# print Cassini location
imglist['POS_KRTP_R'].loc[sampleind]
hemshort = 'N' if hemsph=='North' else 'S'
ax1.text(-0.05, -0.15,
        '{0:.2f} Rs\n{1:.2f}$\degree$ {2}\n{3:.2f} LT'.format(
                imglist['POS_KRTP_R'].loc[sampleind],
                imglist['POS_KRTP_LAT'].loc[sampleind],hemshort,
                imglist['POS_KRTP_LOCT'].loc[sampleind]),
        color='0.4', fontsize=12,
        va='bottom', ha='left', transform=ax1.transAxes)
        
#==============================================================================
# BEAT PHASES
#==============================================================================

# make figure
ax = plt.subplot(gs[0,1], projection='polar')

# plot colormap and colorbar
quad = ax.pcolormesh(theta, r, image.T, cmap=cmap_UV)
quad.set_clim(0, KR_MAX)
quad.set_norm(colors.LogNorm(KR_MIN,KR_MAX))
ax.set_facecolor('gray')

cbar = plt.colorbar(quad, cax=plt.subplot(gs[0,2]), extend='both')
cbar.set_label('Intensity (kR)', rotation=270, labelpad=10)
cbar.set_ticks(np.append(np.append(np.linspace(0.1,0.9,num=9),
                                   np.linspace(1,9,num=9)),
                         np.linspace(10,90,num=9)))

# PPO phase angle
if np.isfinite(ppo_n):
    ax.plot([ppo_n,ppo_n], [0,30], color=myred, lw=2)
    ax.text(ppo_n, 34, 'PPO N', color=myred, fontsize=12, horizontalalignment='center', verticalalignment='center')
    
    for offset in range(1,4):
        angle = (ppo_n+offset*np.pi/2) % (2*np.pi)
        ax.plot([angle,angle], [0,30], color=myred, lw=2, ls=':')
        ax.text(angle, 34, 'PPO N', color=myred, fontsize=12, horizontalalignment='center', verticalalignment='center')
if np.isfinite(ppo_s):
    ax.plot([ppo_s,ppo_s], [0,30], color=myblue, lw=4)
    ax.text(ppo_s+0.08, 35, 'PPO S\n$\Psi_\mathrm{S}=0^\mathrm{o}$', color=myblue,
            fontsize=12, horizontalalignment='center', verticalalignment='center')

# draw grids and annotate lines
ppo_act = (ppo_s) % (2*np.pi)
ppo_lines = np.array([ppo_act-np.pi/4, ppo_act-3*np.pi/4,
                      ppo_act-5*np.pi/4, ppo_act-7*np.pi/4]) % (2*np.pi)
ppo_centers = (ppo_lines+np.pi/4)%(2*np.pi)
linetext = ['$45^\mathrm{o}$','$135^\mathrm{o}$','$225^\mathrm{o}$', '$315^\mathrm{o}$']
rtext = [32,34,32,34]
centertext = ['in phase','S leading N','in antiphase', 'S lagging N']
centerrot = np.array([iii*180/np.pi if (iii<np.pi/2 or iii>3*np.pi/2)
                      else ((iii+np.pi)%(2*np.pi))*180/np.pi
                      for iii in ppo_centers ])
rcenter = [27,27,26,26]
for iii in range(len(ppo_lines)):
    ccc = '0.2' if iii%2 else '0.8'
    tccc = 'k' if iii%2 else 'w'
    ppol = ppo_lines[iii]
    ax.plot([ppol,ppol], [0,30], c=myblue, lw=2, ls='--')
    ax.text(ppol, rtext[iii], linetext[iii], color=myblue,
            fontsize=12, horizontalalignment='center', verticalalignment='center')
    lim2 = ppo_lines[(iii+1)%len(ppo_lines)]
    if lim2<ppol:
        ax.fill_between(np.linspace(lim2, ppol,num=100),0,30,
                        color='white', alpha=0.4*(iii%2))
        ax.fill_between(np.linspace(lim2, ppol,num=100),24,30,
                        color=ccc)
    else:
        ax.fill_between(np.linspace(lim2, 2*np.pi,num=100),0,30,
                        color='white', alpha=0.4*(iii%2))
        ax.fill_between(np.linspace(0, ppol,num=100),0,30,
                        color='white', alpha=0.4*(iii%2))
        ax.fill_between(np.linspace(lim2, 2*np.pi,num=100),24,30,
                        color=ccc)
        ax.fill_between(np.linspace(0, ppol,num=100),24,30,
                        color=ccc)
    ax.text(ppo_centers[iii], rcenter[iii], centertext[iii],ha='center',va='center',
            color=tccc, fontsize=14, rotation=centerrot[iii])
                    
# general figure setup 
ax.set_title('{}\n{}, {:.1f} min'.format(timestr, hemsph, exp/60), fontsize=13, y=1.08)
ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
ticklabels = ['00','06','12','18']
for iii in range(len(ticklabels)):
    txt = ax.text(ticks[iii], 27, ticklabels[iii], color='w', fontsize=16, fontweight='bold',
                  ha='center',va='center')
    txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
ax.set_xticks(ticks)
ax.set_xticklabels([])
ax.set_yticks([10,20,30])
ax.set_yticklabels([])
ax.grid('on', color='0.8', linewidth=1)
ax.set_theta_zero_location("N")
ax.set_rmax(30)
ax.set_rmin(0)

txt = ax.text(0, 1, '(b)',
              transform=ax.transAxes, ha='center', va='center',
              color='k', fontweight='bold', fontsize=20)
txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
        
# save and close
plt.savefig('{}/sample_desc.png'.format(samplepath), bbox_inches='tight', dpi=250)
plt.savefig('{}/sample_desc.pdf'.format(samplepath), bbox_inches='tight', dpi=250)
plt.show()
plt.close()

sys.exit()

#%%
#==============================================================================
# Beat phase modulations
#==============================================================================

# axis, psi angle around which the FAC region is centered, up or down direction
def addFAC(ax, bx, angle, direction, ppotype, hem='North'):
    if hem == 'North':
        ccc = myred if ppotype=='primary' else myblue
    elif hem == 'South':
        ccc = myred if ppotype=='secondary' else myblue
    numcirc = 7
    radii = np.linspace(5,10,num=int((numcirc+1)/2))
    radii = np.append(radii, radii[:-1][::-1])
    
    tmp = np.sort(np.unique(radii))[::-1][:-1]/5
    tmp = np.array([15,7,2])
    tmp = np.array([0,0,0])
    tmp2 = np.append(tmp,[0])
    tmp2 = np.append(tmp2, -tmp[::-1])
    
    ang_offsets = np.linspace(-60,60,num=numcirc)
    ang_offsets += tmp2
     
    angles = np.array([270-(angle+iii) for iii in ang_offsets])%360
    angles = angles/180*np.pi
    
#    radii = [8,10,15,10,8]
    dist = 60 if ppotype=='primary' else 45
    xs = dist*np.cos(angles)
    ys = dist*np.sin(angles)
    for iii in range(len(xs)):
        bx.add_patch(ptch.Circle([xs[iii],ys[iii]], radii[iii]**1.5/3,
                                   color=ccc, fill=False,
                                   linewidth=3))
        if direction=='up':
            bx.add_patch(ptch.Circle([xs[iii],ys[iii]], radii[iii]**1.8/16,
                                       color=ccc))
        else:
            bx.text(xs[iii], ys[iii], 'x', color=ccc,
                    ha='center', va='center',
                    fontweight='bold', fontsize=radii[iii]**1.8/2)
    
    # mark approximate regions of upward current
    if ((direction=='up') & (hem=='North')) or ((direction=='down') & (hem=='South')):
        regmax = 100 if ppotype=='primary' else 75
        minang = ((angle-60)%360)/180*np.pi
        maxang = ((angle+60)%360)/180*np.pi
#        minang = ((angle-30)%360)/180*np.pi
#        maxang = ((angle+30)%360)/180*np.pi
        if maxang>minang:
            ax.fill_between(np.linspace(minang, maxang, num=100),0,regmax,
                            color=ccc, alpha=0.3)
        else:
            ax.fill_between(np.linspace(minang, 2*np.pi, num=100),0,regmax,
                            color=ccc, alpha=0.3)
            ax.fill_between(np.linspace(0, maxang, num=100),0,regmax,
                            color=ccc, alpha=0.3)
    
    
def setupAx(ax, bx, style='N', MAX_R=100):
    ax.set_theta_zero_location('S')
    ax.set_theta_direction(-1)
    ax.set_rlim([0,MAX_R])
    
    xtic = np.arange(0,2*np.pi,np.pi/6)
    xlab = ['$\Psi_\mathrm{%s}=%s^\mathrm{o}$' % (style,
            int(xtic[iii]*180/np.pi)) if not iii%3 else '' for iii in range(len(xtic))]
    ax.set_xticks(xtic)
    ax.set_xticklabels([])
    for iii in range(len(xtic)):
        ax.text(xtic[iii], 107, xlab[iii], color='k',
                fontsize=12, horizontalalignment='center', verticalalignment='center',
                rotation=xtic[iii]*180/np.pi if iii%6 else 'horizontal')
    
    ax.set_yticks([0])
    ax.set_yticklabels([''])
    ax.grid(color='0.4',linestyle='--')
    
    bx.set_xticks([])
    bx.set_yticks([])
    bx.set_xlim([-100,100])
    bx.set_ylim([-100,100])
    
def addPPO(ax,angle,ppotype,hem='North'):
    if hem=='North':
        ccc = myblue if ppotype=='secondary' else myred
        name = 'PPO N' if ppotype=='primary' else 'PPO S'
    elif hem=='South':
        ccc = myblue if ppotype=='primary' else myred
        name = 'PPO N' if ppotype=='secondary' else 'PPO S'
    
    ddd = 62 if ppotype=='primary' else 25
    ppol = angle/180*np.pi
#    ax.plot([ppol,ppol], [0,120], c='k', lw=2, ls='-',zorder=5)
#    ax.fill_between(np.linspace(ppol-0.07,ppol+0.07,num=100), 0,ddd+15,
#                    color=ccc, alpha=0.8, edgecolor='k')
    txt = bx.text(-ddd*np.sin(ppol), -ddd*np.cos(ppol), name, color=ccc, fontsize=13, fontweight='bold',
            va='center', ha='center', zorder=9)
    txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
    dist = 80 if ppotype=='primary' else 50
    ptc = bx.add_patch(
            ptch.FancyArrowPatch(
                    (0,0),
                    (-dist*np.sin(ppol), -dist*np.cos(ppol)),
                     arrowstyle='Fancy,head_length=20, head_width=10, tail_width=5',
                     color=ccc))
    ptc.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
    
def markRegion(ax,minang,maxang):
    minang = minang*np.pi/180
    maxang = maxang*np.pi/180
    if maxang>minang:
        ax.fill_between(np.linspace(minang, maxang, num=100),0,100,
                        facecolor='none', edgecolor='k', hatch='xx', alpha=0.3)
    else:
        ax.fill_between(np.linspace(minang, 2*np.pi, num=100),0,100,
                        facecolor='none', edgecolor='k', hatch='xx', alpha=0.3)
        ax.fill_between(np.linspace(0, maxang, num=100),0,100,
                        facecolor='none', edgecolor='k', hatch='xx', alpha=0.3)
    
fig = plt.figure()
fig.set_size_inches(10,10)

relsize = 0.4
margin = (1-2*relsize)/4

ax1 = fig.add_axes([margin,0.5+margin,relsize,relsize], projection='polar')
ax2 = fig.add_axes([0.5+margin,0.5+margin,relsize,relsize], projection='polar')
ax3 = fig.add_axes([margin,margin,relsize,relsize], projection='polar')
ax4 = fig.add_axes([0.5+margin,margin,relsize,relsize], projection='polar')

bx1 = fig.add_axes([0.00,0.50,0.5,0.5], frameon=False)
bx2 = fig.add_axes([0.50,0.50,0.5,0.5], frameon=False)
bx3 = fig.add_axes([0.00,0.00,0.5,0.5], frameon=False)
bx4 = fig.add_axes([0.50,0.00,0.5,0.5], frameon=False)

axs = [ax1,ax2,ax3,ax4]
bxs = [bx1,bx2,bx3,bx4]

beat_names = ['in phase', 'in antiphase', 'S leading N', 'S lagging N']
beat_offsets = [0,180,270,90]
markmin = [30, None, 30, 120]
markmax = [150, None, 60, 150]

for iii in range(len(axs)):
    ax = axs[iii]
    bx = bxs[iii]
    setupAx(ax, bx)
    addFAC(ax, bx, 90, 'up', 'primary')
    addFAC(ax, bx, 270, 'down', 'primary')
    addPPO(ax,0, 'primary')
    addFAC(ax, bx, 90+beat_offsets[iii], 'up', 'secondary')
    addFAC(ax, bx, 270+beat_offsets[iii], 'down', 'secondary')
    addPPO(ax,beat_offsets[iii], 'secondary')
    ax.set_title(beat_names[iii], y=1.06, fontweight='bold', fontsize=12)
    if markmin[iii]:
        markRegion(ax, markmin[iii], markmax[iii])

ax1.text(0,1,'(a)',transform=ax1.transAxes, fontsize=20, fontweight='bold')
ax2.text(0,1,'(b)',transform=ax2.transAxes, fontsize=20, fontweight='bold')
ax3.text(0,1,'(c)',transform=ax3.transAxes, fontsize=20, fontweight='bold')
ax4.text(0,1,'(d)',transform=ax4.transAxes, fontsize=20, fontweight='bold')

#fig.suptitle('Northern hemisphere FACs', y=1)

plt.savefig('{}/beat_desc.pdf'.format(samplepath), bbox_inches='tight', dpi=250)
plt.savefig('{}/beat_desc.png'.format(samplepath), bbox_inches='tight', dpi=250)
plt.show()
plt.close()

#%%
#===================
# small model thing

def add_flows(ax, cent_theta_deg, ccw=False):
    color = 'r'
    color = myblue
    if not ccw:
        z = np.linspace(0,2*np.pi, num=500)
    else:
        z = np.linspace(2*np.pi,0, num=500)
    c_r = 72
    w_r = 35
    c_t = np.radians(cent_theta_deg)
    w_t = 5/6*np.pi
    r = np.cos(z)*w_r/2+c_r-np.heaviside(-np.cos(z),0)*np.abs(np.cos(z)*w_r)
    t = np.sin(z)*w_t/2+c_t
    valid = np.where((z<np.radians(95)) | (z>np.radians(265)))[0]
    ax.plot(t[valid], r[valid], c=color, lw=2)
    diff = 2
    cent = np.array([30,300,470])+5
    arrowstyle = '->,head_length=10, head_width=5'
    for iii in range(3):
        frac = 0.84 if iii==1 else 1
        ax.add_patch(ptch.FancyArrowPatch((t[int(cent[iii]-diff)], r[int(cent[iii]-diff)]*frac),
                                           (t[int(cent[iii])], r[int(cent[iii])]*frac),
                                           arrowstyle=arrowstyle,
                                           color=color,
                                           linewidth=3))

for direc in ['h','v']:
    
    fig = plt.figure()

    if direc == 'h':
        fig.set_size_inches(8,4)
        ax1 = fig.add_axes([0,0,0.45,1], projection='polar')
        bx1 = fig.add_axes([0,0,0.45,1], frameon=False)
        ax2 = fig.add_axes([0.55,0,0.45,1], projection='polar')
        bx2 = fig.add_axes([0.55,0,0.45,1], frameon=False)
    elif direc == 'v':
        fig.set_size_inches(4,8)
        ax1 = fig.add_axes([0,0.55,1,0.45], projection='polar')
        bx1 = fig.add_axes([0,0.55,1,0.45], frameon=False)
        ax2 = fig.add_axes([0,0,1,0.45], projection='polar')
        bx2 = fig.add_axes([0,0,1,0.45], frameon=False)
    bx1.axis('equal')
    bx2.axis('equal')
    
    # some stuff for arrows
    minang = 65/180*np.pi
    maxang = 25/180*np.pi
    style="Simple,tail_width=0.5,head_width=4,head_length=8"
    kw = dict(arrowstyle=style, color="k")
    rarr = 108 if direc=='h' else 97
    
    # plot
    setupAx(ax1, bx1, style='N/S')
    addFAC(ax1, bx1, 90, 'up', 'primary')
    addFAC(ax1, bx1, 270, 'down', 'primary')
    ax1.text(0.3,0.5,'up', color='k', fontsize=15, fontweight='bold',
             va='center', ha='center', transform=ax1.transAxes)
    ax1.text(0.7,0.5,'down', color='k', fontsize=15, fontweight='bold',
             va='center', ha='center', transform=ax1.transAxes)
    add_flows(ax1, 90)
    add_flows(ax1, 270, ccw=True)
    bx1.add_patch(ptch.FancyArrowPatch((rarr*np.cos(minang), -rarr*np.sin(minang)),
                                       (rarr*np.cos(maxang), -rarr*np.sin(maxang)),
                                       connectionstyle="arc3,rad=.2", **kw))
    bx1.add_patch(ptch.FancyArrowPatch((0, 80),
                                       (0, -80),
                                       arrowstyle='Fancy,head_length=20, head_width=10, tail_width=5',
                                       color='k'))
    ax1.text(0.94,0.06,'Patterns rotate\n with' + r' $\tau_\mathrm{N}$ and $\tau_\mathrm{S}$', color='k', fontsize=12, fontstyle='italic',
             va='center', ha='center', transform=ax1.transAxes, rotation=45)
    
    setupAx(ax2, bx2, style='N/S')
    addFAC(ax2, bx2, 90, 'up', 'primary', hem='South')
    addFAC(ax2, bx2, 270, 'down', 'primary', hem='South')
    ax2.text(0.3,0.5,'down', color='k', fontsize=15, fontweight='bold',
             va='center', ha='center', transform=ax2.transAxes)
    ax2.text(0.7,0.5,'up', color='k', fontsize=15, fontweight='bold',
             va='center', ha='center', transform=ax2.transAxes)
    add_flows(ax2, 90, ccw=True)
    add_flows(ax2, 270)
    bx2.add_patch(ptch.FancyArrowPatch((rarr*np.cos(minang), -rarr*np.sin(minang)),
                                       (rarr*np.cos(maxang), -rarr*np.sin(maxang)),
                                       connectionstyle="arc3,rad=.2", **kw))
    bx2.add_patch(ptch.FancyArrowPatch((0, 80),
                                       (0, -80),
                                       arrowstyle='Fancy,head_length=20, head_width=10, tail_width=5',
                                       color='k'))
    ax2.text(0.94,0.06,'Patterns rotate\n with' + r' $\tau_\mathrm{N}$ and $\tau_\mathrm{S}$', color='k', fontsize=12, fontstyle='italic',
             va='center', ha='center', transform=ax2.transAxes, rotation=45)
    
    ax1.set_title('Northern hemisphere (viewed from N)', y=1.06, fontweight='bold', fontsize=12)
    ax2.set_title('Southern hemisphere (viewed from N)', y=1.06, fontweight='bold', fontsize=12)
    
    plt.savefig('{}/general_model_{}.pdf'.format(samplepath, direc), bbox_inches='tight', dpi=250)
    plt.savefig('{}/general_model_{}.png'.format(samplepath, direc), bbox_inches='tight', dpi=250)
    plt.show()
    plt.close()
        
print('Done')