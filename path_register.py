# Paths
import socket

class PathRegister(object):
    
    def __init__(self):
        self.hostname = socket.gethostname()
        
        if self.hostname == 'Thor':
            self.mainpath = '/home/alex/PhD'
        
        elif self.hostname == 'Yggdrasil':
            self.mainpath = 'D:/PhD'
                        
        elif self.hostname == 'pyb504000032':
            self.mainpath = 'E:'
            
        self.datapath = '%s/data' % self.mainpath
        self.uvispath = '{}/UVIS/PDS_UVIS/PROJECTIONS_RES2_ISS'.format(self.datapath)
        
        self.gitpath = '%s/GIT' % self.mainpath
        
        self.boxpath = '%s/Box Sync' % self.mainpath
        self.plotpath = '%s/Plots' % self.boxpath
        self.imglistpath = '%s/imglists' % self.boxpath
        