# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)

import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import numpy as np
import os
import time
import time_conversions as tc
import uvisdb

udb = uvisdb.UVISDB(update=False)
imglist = udb.currentDF.copy()
imglist['IMGTIME'] = imglist[['ET_START','ET_STOP']].mean(axis=1)

# determine beat phase for all images        
# calculate PPO beat phase
imglist['PPO_SminN'] = (imglist['PPO_PHASE_S'] - imglist['PPO_PHASE_N']) % (2*np.pi)
imglist['PPO_BEAT_PHASE'] = 0
# in phase
imglist['PPO_BEAT_PHASE'][(imglist['PPO_SminN']<=1/4*np.pi) |
                            (imglist['PPO_SminN']>7/4*np.pi)] = 1
# S leading N
imglist['PPO_BEAT_PHASE'][(imglist['PPO_SminN']>1/4*np.pi) &
                            (imglist['PPO_SminN']<=3/4*np.pi)] = 2
# antiphase
imglist['PPO_BEAT_PHASE'][(imglist['PPO_SminN']>3/4*np.pi) &
                            (imglist['PPO_SminN']<=5/4*np.pi)] = 3
# S lagging N
imglist['PPO_BEAT_PHASE'][(imglist['PPO_SminN']>5/4*np.pi) &
                            (imglist['PPO_SminN']<=7/4*np.pi)] = 4
       
beatphase = ['00 all','01 in phase','02 S leading N','03 in antiphase','04 S lagging N']
cmaps = [plt.cm.Greys,plt.cm.Blues,plt.cm.Reds,plt.cm.Greens,plt.cm.Oranges]
colors = ['k','b','r','g','darkorange']
myred = 'crimson'
myblue = 'royalblue'

# which images correspond to which subinterval
datefmt = '%Y-%jT%H-%M-%S'
subintervnames = ['SUBINT_all', 'SUBINT_outside_coalescence', 'SUBINT_during_coalescence']
for iii in range(len(subintervnames)):
    current = subintervnames[iii]
    imglist[current] = 0
    if current == 'SUBINT_all':
        imglist[current] = 1
    elif current == 'SUBINT_outside_coalescence':
        imglist[current][(imglist['IMGTIME'] <= tc.str2et('2013-181T00-00-00', datefmt)) | 
                         (imglist['IMGTIME'] >= tc.str2et('2014-181T00-00-00', datefmt))] = 1
    elif current == 'SUBINT_during_coalescence':
        imglist[current][(imglist['IMGTIME'] > tc.str2et('2013-181T00-00-00', datefmt)) & 
                         (imglist['IMGTIME'] < tc.str2et('2014-181T00-00-00', datefmt))] = 1
               
imglist['BOUNDARY_VALID'] = True
imglist.at[imglist['POS_KRTP_R'] > 40,'BOUNDARY_VALID'] = False
#imglist.at[imglist['EXP'] < 500,'BOUNDARY_VALID'] = False
# exclude a bunch of weird images
imglist.at[(imglist['YEAR']==2013)&(imglist['DOY']>=233)&(imglist['DOY']<=239),'BOUNDARY_VALID'] = False
imglist.at[(imglist['YEAR']==2017)&(imglist['DOY']>=196)&(imglist['DOY']<=197),'BOUNDARY_VALID'] = False
imglist.at[(imglist['YEAR']==2017)&(imglist['DOY']>=236)&(imglist['DOY']<=237),'BOUNDARY_VALID'] = False
imglist.at[(imglist['YEAR']==2017)&(imglist['DOY']>=257)&(imglist['DOY']<=257),'BOUNDARY_VALID'] = False

# calculate median LT centers
hems = ['North','South']
# [beatphase, hem, pole/cent/eq, med/MAD]
LT_med_cv = np.zeros((5,2,3,2,)+np.shape(imglist['OPENCV_CENTERS'].iloc[0]))
for bctr in range(len(beatphase)):
    for hctr in range(len(hems)):
        for pce in range(3):
            if pce==0:
                tag = 'OPENCV_POLEW'
            elif pce==1:
                tag = 'OPENCV_CENTERS'
            elif pce==2:
                tag = 'OPENCV_EQW'
            tmp = np.concatenate([imglist[(imglist['HEMISPHERE']==hems[hctr]) &
                                          (imglist['PPO_BEAT_PHASE']>=0 if bctr==0
                                           else imglist['PPO_BEAT_PHASE']==bctr) &
                                          (imglist['BOUNDARY_VALID']==True)][tag].tolist()])
            # median
            LT_med_cv[bctr,hctr,pce,0,:] = np.nanmedian(tmp, axis=0)
            # MAD
            LT_med_cv[bctr,hctr,pce,1,:] = np.nanmedian(np.abs(tmp-np.nanmedian(tmp, axis=0)), axis=0)
            
# calculate median LT centers for seasons pre/post 2011
hems = ['North','South']
# [season, hem, pole/cent/eq, med/MAD]
LT_med_cv_seas = np.zeros((2,2,3,2,)+np.shape(imglist['OPENCV_CENTERS'].iloc[0]))
for sctr in range(2):
    for hctr in range(len(hems)):
        for pce in range(3):
            if pce==0:
                tag = 'OPENCV_POLEW'
            elif pce==1:
                tag = 'OPENCV_CENTERS'
            elif pce==2:
                tag = 'OPENCV_EQW'
            tmp = np.concatenate([imglist[(imglist['HEMISPHERE']==hems[hctr]) &
                                          (imglist['PPO_BEAT_PHASE']>=0 if bctr==0
                                           else imglist['PPO_BEAT_PHASE']==bctr) &
                                          (imglist['BOUNDARY_VALID']==True) &
                                          ((imglist['YEAR']<=2011) if not sctr else True) &
                                          ((imglist['YEAR']>2011) if sctr else True)
                                          ][tag].tolist()])
            # median
            LT_med_cv_seas[sctr,hctr,pce,0,:] = np.nanmedian(tmp, axis=0)
            # MAD
            LT_med_cv_seas[sctr,hctr,pce,1,:] = np.nanmedian(np.abs(tmp-np.nanmedian(tmp, axis=0)), axis=0)
            
# calculate median LT centers for solar cycle hi/lo
sc_lo = [2007,2008,2009,2016,2017]
sc_hi = [2012,2013,2014]
imglist['SC_LO'] = [imglist['YEAR'].loc[iii] in sc_lo for iii in imglist.index]
imglist['SC_HI'] = [imglist['YEAR'].loc[iii] in sc_hi for iii in imglist.index]
hems = ['North','South']
# [sc lo/hi, hem, pole/cent/eq, med/MAD]
LT_med_cv_sc = np.zeros((2,2,3,2,)+np.shape(imglist['OPENCV_CENTERS'].iloc[0]))
for sctr in range(2):
    for hctr in range(len(hems)):
        for pce in range(3):
            if pce==0:
                tag = 'OPENCV_POLEW'
            elif pce==1:
                tag = 'OPENCV_CENTERS'
            elif pce==2:
                tag = 'OPENCV_EQW'
            tmp = np.concatenate([imglist[(imglist['HEMISPHERE']==hems[hctr]) &
                                          (imglist['PPO_BEAT_PHASE']>=0 if bctr==0
                                           else imglist['PPO_BEAT_PHASE']==bctr) &
                                          (imglist['BOUNDARY_VALID']==True) &
                                          ((imglist['SC_LO']==True) if not sctr else True) &
                                          ((imglist['SC_HI']==True) if sctr else True)
                                          ][tag].tolist()])
            # median
            LT_med_cv_sc[sctr,hctr,pce,0,:] = np.nanmedian(tmp, axis=0)
            # MAD
            LT_med_cv_sc[sctr,hctr,pce,1,:] = np.nanmedian(np.abs(tmp-np.nanmedian(tmp, axis=0)), axis=0)
              
# lon binning  
tmp = np.linspace(0,360,num=np.shape(LT_med_cv)[-1]+1)
loncenters = tmp[:-1] + np.diff(tmp)/2
#tmp = np.linspace(0,360,num=np.shape(LT_med)[-1]/2+1)
#tmp = tmp[:-1] + np.diff(tmp)/2
xdata = loncenters/180*np.pi
xdata = np.append([0],xdata)
xdata = np.append(xdata, [2*np.pi])

# rotate boundaries into PPO N and S magnetic longitude, with and without season
print('Rotating boundaries into PPO magnetic longitude')
imglist['CV_LATS_PPO_N'] = [np.zeros((len(loncenters)))]*len(imglist)
imglist['CV_LATS_PPO_S'] = [np.zeros((len(loncenters)))]*len(imglist)
imglist['CV_LATS_PPO_N_seas'] = [np.zeros((len(loncenters)))]*len(imglist)
imglist['CV_LATS_PPO_S_seas'] = [np.zeros((len(loncenters)))]*len(imglist)
imglist['CV_LATS_PPO_N_sc'] = [np.zeros((len(loncenters)))]*len(imglist)
imglist['CV_LATS_PPO_S_sc'] = [np.zeros((len(loncenters)))]*len(imglist)
current = 0
for imgctr in imglist[imglist['BOUNDARY_VALID']].index:
    if imgctr-100 > current:
        current = (imgctr // 100)*100
        print(imgctr, '/', len(imglist))
    thisbeat = imglist['PPO_BEAT_PHASE'].loc[imgctr]
    thishem = 0 if imglist['HEMISPHERE'].loc[imgctr] == 'North' else 1
        
    # ==============
    # CV boundaries
    
    # subtract LT average
    tmp = imglist['OPENCV_CENTERS'].loc[imgctr]
    tmp[imglist['OPENCV_ELEVS'].loc[imgctr]<25] = np.nan
    offsets = tmp-LT_med_cv[0,thishem,1,0,:]
    # seasonal
    if imglist['YEAR'].loc[imgctr] <= 2011:
        offsets_seas = tmp-LT_med_cv_seas[0,thishem,1,0,:]
    else:
        offsets_seas = tmp-LT_med_cv_seas[1,thishem,1,0,:]
    # solar cycle
    if imglist['YEAR'].loc[imgctr] in sc_lo:
        offsets_sc = tmp-LT_med_cv_sc[0,thishem,1,0,:]
    elif imglist['YEAR'].loc[imgctr] in sc_hi:
        offsets_sc = tmp-LT_med_cv_sc[1,thishem,1,0,:]
    else:
        offsets_sc = tmp-LT_med_cv[0,thishem,1,0,:]
    
    # rotate into PPO N
    refangle = imglist['PPO_PHASE_N'].loc[imgctr]
    refangle = (refangle*180/np.pi) % 360
    imglist['CV_LATS_PPO_N'].loc[imgctr] = np.roll(np.flip(offsets,axis=-1),
                          np.around((refangle/360*len(loncenters))).astype(int),axis=-1)
    imglist['CV_LATS_PPO_N_seas'].loc[imgctr] = np.roll(np.flip(offsets_seas,axis=-1),
                          np.around((refangle/360*len(loncenters))).astype(int),axis=-1)
    imglist['CV_LATS_PPO_N_sc'].loc[imgctr] = np.roll(np.flip(offsets_sc,axis=-1),
                          np.around((refangle/360*len(loncenters))).astype(int),axis=-1)
    
    # rotate into PPO S
    refangle = imglist['PPO_PHASE_S'].loc[imgctr]
    refangle = (refangle*180/np.pi) % 360
    imglist['CV_LATS_PPO_S'].loc[imgctr] = np.roll(np.flip(offsets,axis=-1),
                          np.around((refangle/360*len(loncenters))).astype(int),axis=-1)
    imglist['CV_LATS_PPO_S_seas'].loc[imgctr] = np.roll(np.flip(offsets_seas,axis=-1),
                          np.around((refangle/360*len(loncenters))).astype(int),axis=-1)
    imglist['CV_LATS_PPO_S_sc'].loc[imgctr] = np.roll(np.flip(offsets_sc,axis=-1),
                          np.around((refangle/360*len(loncenters))).astype(int),axis=-1)

#imglist['AVG_OFFS_N'] = 0.
#imglist['AVG_OFFS_S'] = 0.
#for iii in imglist.index:
#    imglist.at[iii, 'AVG_OFFS_N'] = np.nanmean(imglist.loc[iii,'CV_LATS_PPO_N'])
#    imglist.at[iii, 'AVG_OFFS_S'] = np.nanmean(imglist.loc[iii,'CV_LATS_PPO_S'])
#tmp = imglist[(imglist['HEMISPHERE']=='North') & (imglist['PPO_BEAT_PHASE']==4) & (imglist['BOUNDARY_VALID'])]
#tmp.groupby(by=('YEAR', 'DOY')).count()['AVG_OFFS_N']
        
# path for saving plots
mainsavepath = '{}/Plots/stat_bound/{}'.format(boxpath, time.strftime('%Y%m%d%H%M%S'))
if not os.path.exists(mainsavepath):
    os.makedirs(mainsavepath)
    
# hemisphere, beat phase, med/mad, lon
comp_data_cv = np.zeros((2,5,2,2,len(xdata)))
comp_data_cv_seas = np.zeros((2,5,2,2,len(xdata)))
comp_data_cv_sc = np.zeros((2,5,2,2,len(xdata)))
comp_num_cv = np.zeros((2,5,2))
comp_num_cv_seas = np.zeros((2,5,2))
comp_num_cv_sc = np.zeros((2,5,2))

for bctr in range(len(beatphase)):
    thisbeat = beatphase[bctr]
    for rctr in ['LT','primary','secondary']:
        for hemctr in range(len(hems)):
            thishem = hems[hemctr]
            selec = imglist[(imglist['HEMISPHERE'] == thishem) &
                            (imglist['BOUNDARY_VALID']==True) &
                            (imglist['SUBINT_outside_coalescence']==True) &
                            (imglist['PPO_BEAT_PHASE']>=0 if bctr==0
                             else imglist['PPO_BEAT_PHASE']==bctr)]
            if rctr == 'LT':
                cv_key = 'OPENCV_CENTERS'
            else:
                if rctr == 'primary':
                    cv_key = 'CV_LATS_PPO_{}'.format(thishem[0])
                if rctr == 'secondary':
                    cv_key = 'CV_LATS_PPO_{}'.format('N' if thishem[0] == 'S' else 'S')
            data_cv = np.concatenate([selec[cv_key].tolist()])
                        
            # insert extra points at 0 and 360
            tmp = np.nanmean(data_cv[:,::np.shape(data_cv)[-1]-1], axis=-1)
            data_cv = np.concatenate([np.expand_dims(tmp, axis=-1),
                                      data_cv,
                                      np.expand_dims(tmp, axis=-1)], axis=-1)
                        
            # average
            bound_med_cv = np.nanmedian(data_cv, axis=0)
            bound_mad_cv = np.nanmedian(np.abs(bound_med_cv-data_cv), axis=0)
            
            if rctr == 'primary':
                comp_data_cv[hemctr,bctr,0,0,:] = bound_med_cv
                comp_data_cv[hemctr,bctr,0,1,:] = bound_mad_cv
                comp_num_cv[hemctr,bctr,0] = np.sum(np.isfinite(data_cv[:,1:-1]))
            elif rctr == 'secondary':
                comp_data_cv[hemctr,bctr,1,0,:] = bound_med_cv
                comp_data_cv[hemctr,bctr,1,1,:] = bound_mad_cv
                comp_num_cv[hemctr,bctr,1] = np.sum(np.isfinite(data_cv[:,1:-1]))
                
            
            cv_key_orig = np.copy(cv_key)
            
            #--------------------
            # get seasonal stuff
            if not rctr == 'LT':
                cv_key = '{}_seas'.format(cv_key_orig)
            data_cv = np.concatenate([selec[cv_key].tolist()])
                        
            # insert extra points at 0 and 360
            tmp = np.nanmean(data_cv[:,::np.shape(data_cv)[-1]-1], axis=-1)
            data_cv = np.concatenate([np.expand_dims(tmp, axis=-1),
                                      data_cv,
                                      np.expand_dims(tmp, axis=-1)], axis=-1)
                        
            # average
            bound_med_cv = np.nanmedian(data_cv, axis=0)
            bound_mad_cv = np.nanmedian(np.abs(bound_med_cv-data_cv), axis=0)
            
            if rctr == 'primary':
                comp_data_cv_seas[hemctr,bctr,0,0,:] = bound_med_cv
                comp_data_cv_seas[hemctr,bctr,0,1,:] = bound_mad_cv
                comp_num_cv_seas[hemctr,bctr,0] = np.sum(np.isfinite(data_cv[:,1:-1]))
            elif rctr == 'secondary':
                comp_data_cv_seas[hemctr,bctr,1,0,:] = bound_med_cv
                comp_data_cv_seas[hemctr,bctr,1,1,:] = bound_mad_cv
                comp_num_cv_seas[hemctr,bctr,1] = np.sum(np.isfinite(data_cv[:,1:-1]))
                
            #--------------------
            # get solar cycle stuff
            if not rctr == 'LT':
                cv_key = '{}_sc'.format(cv_key_orig)
            data_cv = np.concatenate([selec[cv_key].tolist()])
                        
            # insert extra points at 0 and 360
            tmp = np.nanmean(data_cv[:,::np.shape(data_cv)[-1]-1], axis=-1)
            data_cv = np.concatenate([np.expand_dims(tmp, axis=-1),
                                      data_cv,
                                      np.expand_dims(tmp, axis=-1)], axis=-1)
                        
            # average
            bound_med_cv = np.nanmedian(data_cv, axis=0)
            bound_mad_cv = np.nanmedian(np.abs(bound_med_cv-data_cv), axis=0)
            
            if rctr == 'primary':
                comp_data_cv_sc[hemctr,bctr,0,0,:] = bound_med_cv
                comp_data_cv_sc[hemctr,bctr,0,1,:] = bound_mad_cv
                comp_num_cv_sc[hemctr,bctr,0] = np.sum(np.isfinite(data_cv[:,1:-1]))
            elif rctr == 'secondary':
                comp_data_cv_sc[hemctr,bctr,1,0,:] = bound_med_cv
                comp_data_cv_sc[hemctr,bctr,1,1,:] = bound_mad_cv
                comp_num_cv_sc[hemctr,bctr,1] = np.sum(np.isfinite(data_cv[:,1:-1]))

comp_data_cv_noseas = np.copy(comp_data_cv)
comp_num_cv_noseas = np.copy(comp_num_cv)

for tag in ['seas','noseas','sc']:
    if tag=='seas':
        comp_data_cv = comp_data_cv_seas
        comp_num_cv = comp_num_cv_seas
    elif tag=='noseas':
        comp_data_cv = comp_data_cv_noseas
        comp_num_cv = comp_num_cv_noseas
    else:
        comp_data_cv = comp_data_cv_sc
        comp_num_cv = comp_num_cv_sc        

    # ===================
    # primary-secondary comparison     
    fig = plt.figure()
    fig.set_size_inches(12,6)
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122, sharey=ax1)
    axes = [ax1,ax2]
    for hemctr in range(len(hems)):
        thishem = hems[hemctr]
        ax = axes[hemctr]    
        # primary
        ax.plot(xdata, comp_data_cv[hemctr,0,0,0,:], c='k', ls='-', lw=3,
                label='primary ({})'.format(int(comp_num_cv[hemctr,0,0])))
        ax.fill_between(xdata,
                        comp_data_cv[hemctr,0,0,0,:]-comp_data_cv[hemctr,0,0,1,:],
                        comp_data_cv[hemctr,0,0,0,:]+comp_data_cv[hemctr,0,0,1,:],
                        color='k',
                        alpha=0.4)    
        # secondary
        ax.plot(xdata, comp_data_cv[hemctr,0,1,0,:], c='k', ls='-', lw=1,
                label='secondary ({})'.format(int(comp_num_cv[hemctr,0,1])))
        ax.fill_between(xdata,
                        comp_data_cv[hemctr,0,1,0,:]-comp_data_cv[hemctr,0,1,1,:],
                        comp_data_cv[hemctr,0,1,0,:]+comp_data_cv[hemctr,0,1,1,:],
                        color='k',
                        alpha=0.2)    
        # settings
        ax.grid('on')
        ax.set_xlim([0,2*np.pi])
        ax.set_xticks(np.arange(0,5)*np.pi/2)
        ax.set_xticklabels([0,90,180,270,360])
        ax.set_xlabel(r'$\Psi_\mathrm{{\mathbf{{{0}}}/{1}}} = \Phi_\mathrm{{\mathbf{{{0}}}/{1}}} - \varphi$ (deg)'.format(thishem[0], 'N' if thishem[0]=='S' else 'S'))
        if thishem=='North':
            exmax = 1/2*np.pi
            disp = 1
        else:
            exmax = 3/2*np.pi
            disp = -1
        ax.axvline(exmax, linewidth=2, linestyle='--', color='0.5')
        ax.text(0.015,0.93,'(a) {}'.format(thishem),
                transform=ax.transAxes, fontweight='bold', fontsize=14)
    ax1.set_ylabel('Colatitude offset (deg)\n (-/+ poleward/equatorward)')
    ax1.legend(loc=3)
    ax2.legend(loc=4)
    ax1.tick_params(direction='in',bottom=True,left=True,top=True,right=True,
            labelleft=True,labelright=False)
    ax2.tick_params(direction='in',bottom=True,left=True,top=True,right=True,
                    labelleft=False,labelright=False)
    plt.subplots_adjust(wspace=0.08)
    
    plt.savefig('{}/bounds_comp_ps_{}.png'.format(mainsavepath, tag),
                bbox_inches='tight', dpi=500)
    plt.savefig('{}/bounds_comp_ps_{}.pdf'.format(mainsavepath, tag),
                bbox_inches='tight', dpi=500)
    plt.show()
    plt.close()
    
    
    # ===================
    # beat phase comparison
    fig = plt.figure()
    fig.set_size_inches(12,6)
    ax1 = fig.add_subplot(121)
    ax2 = fig.add_subplot(122, sharey=ax1)
    axes = [ax1,ax2]
    
    for hemctr in range(len(hems)):
        thishem = hems[hemctr]
        ax = axes[hemctr]
        
        # center
        for bctr in range(len(beatphase)):
            ax.plot(xdata, comp_data_cv[hemctr,bctr,0,0,:], c=colors[bctr], ls='-',
                    label='{} ({})'.format(beatphase[bctr][3:], int(comp_num_cv[hemctr,bctr,0])))
        # settings
        ax.grid('on')
        ax.set_xlim([0,2*np.pi])
        ax.set_xticks(np.arange(0,5)*np.pi/2)
        ax.set_xticklabels([0,90,180,270,360])
        ax.set_xlabel(r'$\Psi_\mathrm{{{0}}} = \Phi_\mathrm{{{0}}}- \varphi$ (deg)'.format(thishem[0]))
        if thishem=='North':
            exmax = 1/2*np.pi
            disp = 1
        else:
            exmax = 3/2*np.pi
            disp = -1
        ax.axvline(exmax, linewidth=2, linestyle='--', color='0.5')
    #    ax.annotate('expected\n max intensity', xy=(exmax, 2*disp), xytext=(exmax+disp*0.8, 2*disp),
    #                ha='left' if rctr==1 else 'right', va='center',
    #                arrowprops=dict(facecolor='0.5', shrink=0.02))
        ax.text(0.015,0.93,'(a) {}'.format(thishem),
                transform=ax.transAxes, fontweight='bold', fontsize=14)
    ax1.set_ylabel('Colatitude offset (deg)\n (-/+ poleward/equatorward)')
    ax1.legend(loc=4)
    ax2.legend(loc=8)
    ax1.tick_params(direction='in',bottom=True,left=True,top=True,right=True,
            labelleft=True,labelright=False)
    ax2.tick_params(direction='in',bottom=True,left=True,top=True,right=True,
                    labelleft=False,labelright=False)
    plt.subplots_adjust(wspace=0.08)
    
    plt.savefig('{}/bounds_comp_cv_{}.png'.format(mainsavepath, tag),
                bbox_inches='tight', dpi=500)
    plt.savefig('{}/bounds_comp_cv_{}.pdf'.format(mainsavepath, tag),
                bbox_inches='tight', dpi=500)
    plt.show()
    plt.close()


#=================
# Paper boundaries
#=============================
# Nichols 2015 boundaries
x_nic = np.arange(5,356,10)
x_nic = x_nic/180*np.pi
y_nic_eq_s = np.array([np.nan,np.nan,np.nan,np.nan,
                       np.nan,np.nan,np.nan,np.nan,
                       18.1,18.2,18.1,18.2,
                       18.3,18.2,18.0,17.9,
                       17.1,16.7,16.7,16.7,
                       16.3,16.6,16.3,16.3,
                       16.5,16.8,17.2,16.9,
                       np.nan,np.nan,np.nan,np.nan,
                       np.nan,np.nan,np.nan,np.nan])
y_nic_pol_s = np.array([np.nan,np.nan,np.nan,np.nan,
                        np.nan,np.nan,np.nan,np.nan,
                        15.1,15.4,15.5,15.2,
                        15.4,15.4,14.8,14.6,
                        13.5,12.8,13.0,12.4,
                        11.2,10.5,9.9,11.2,
                        10.7,11.7,12.9,12.5,
                        np.nan,np.nan,np.nan,np.nan,
                        np.nan,np.nan,np.nan,np.nan])
y_nic_eq_n = np.array([np.nan,np.nan,np.nan,np.nan,
                       np.nan,np.nan,16.7,16.5,
                       16.0,15.8,15.8,15.6,
                       15.4,15.2,14.9,14.7,
                       12.8,12.7,12.5,12.2,
                       11.9,12.0,12.2,11.8,
                       12.2,13.7,14.3,15.5,
                       15.9,17.7,17.2,np.nan,
                       np.nan,np.nan,np.nan,np.nan])
y_nic_pol_n = np.array([np.nan,np.nan,np.nan,np.nan,
                       np.nan,np.nan,12.8,12.8,
                       13.0,13.4,13.1,13.1,
                       12.6,12.3,11.7,11.1,
                       8.2,7.2,6.7,6.2,
                       6.3,5.9,6.5,6.6,
                       6.9,8.3,8.3,9.1,
                       10.6,12.5,13.1,np.nan,
                       np.nan,np.nan,np.nan,np.nan])
    

# Kinrade 2018 boundaries
x_kin = np.arange(5,356,10)
x_kin = x_kin/180*np.pi
y_kin_eq = np.array([np.nan,np.nan,np.nan,np.nan,
                     18.8,18.0,17.1,16.3,
                     16.0,15.7,15.4,15.4,
                     15.0,14.8,14.5,13.4,
                     11.3,11.4,11.6,11.1,
                     11.7,9.90,12.1,10.3,
                     11.9,11.8,13.6,15.0,
                     15.3,16.6,17.0,18.6,
                     np.nan,np.nan,np.nan,np.nan])
y_kin_pol = np.array([np.nan,np.nan,np.nan,np.nan,
                      15.3,15.2,15.0,14.5,
                      15.2,13.9,13.6,13.4,
                      12.7,12.8,11.3,10.3,
                      7.9,8.1,8.4,8.1,
                      8.3,7.8,8.3,6.3,
                      8.2,8.8,11.0,12.3,
                      12.5,12.8,12.6,14.0,
                      np.nan,np.nan,np.nan,np.nan])


#===========
# LT pattern
fig = plt.figure()
fig.set_size_inches(12,17)
gs = gridspec.GridSpec(3,2, wspace=0.05, hspace=0.05)
ax1 = plt.subplot(gs[0,0])
ax2 = plt.subplot(gs[0,1], sharex=ax1, sharey=ax1)
ax3 = plt.subplot(gs[1,0], sharex=ax1, sharey=ax1)
ax4 = plt.subplot(gs[1,1], sharex=ax1, sharey=ax1)
ax5 = plt.subplot(gs[2,0], sharex=ax1, sharey=ax1)
ax6 = plt.subplot(gs[2,1], sharex=ax1, sharey=ax1)
axes = [ax1,ax2,ax3,ax4,ax5,ax6]

tmp = np.mean([LT_med_cv[:,:,:,:,0], LT_med_cv[:,:,:,:,-1]], axis=0)
plot_LT = np.append(np.expand_dims(tmp, axis=-1), LT_med_cv, axis=-1)
plot_LT = np.append(plot_LT, np.expand_dims(tmp, axis=-1), axis=-1)

tmp = np.mean([LT_med_cv_seas[:,:,:,:,0], LT_med_cv_seas[:,:,:,:,-1]], axis=0)
plot_LT_seas = np.append(np.expand_dims(tmp, axis=-1), LT_med_cv_seas, axis=-1)
plot_LT_seas = np.append(plot_LT_seas, np.expand_dims(tmp, axis=-1), axis=-1)

tmp = np.mean([LT_med_cv_sc[:,:,:,:,0], LT_med_cv_sc[:,:,:,:,-1]], axis=0)
plot_LT_sc = np.append(np.expand_dims(tmp, axis=-1), LT_med_cv_sc, axis=-1)
plot_LT_sc = np.append(plot_LT_sc, np.expand_dims(tmp, axis=-1), axis=-1)

panels = 'abcdef'

for hemctr in range(len(hems)):
    thishem = hems[hemctr]
    selec = imglist[(imglist['HEMISPHERE'] == thishem) &
                    (imglist['BOUNDARY_VALID']==True)]
    cv_key = 'OPENCV_CENTERS'
    data_cv = np.concatenate([selec[cv_key].tolist()])
    num_bounds = np.sum(np.isfinite(data_cv))
    
    selec = imglist[(imglist['HEMISPHERE'] == thishem) &
                    (imglist['BOUNDARY_VALID']==True) &
                    (imglist['YEAR']<=2011)]
    data_cv = np.concatenate([selec['OPENCV_CENTERS'].tolist()])
    num_bounds_pre = np.sum(np.isfinite(data_cv))

    selec = imglist[(imglist['HEMISPHERE'] == thishem) &
                    (imglist['BOUNDARY_VALID']==True) &
                    (imglist['YEAR']>2011)]
    data_cv = np.concatenate([selec['OPENCV_CENTERS'].tolist()])
    num_bounds_post = np.sum(np.isfinite(data_cv))
    
    selec = imglist[(imglist['HEMISPHERE'] == thishem) &
                    (imglist['BOUNDARY_VALID']==True) &
                    (imglist['SC_LO']==True)]
    data_cv = np.concatenate([selec['OPENCV_CENTERS'].tolist()])
    num_bounds_lo = np.sum(np.isfinite(data_cv))
    
    selec = imglist[(imglist['HEMISPHERE'] == thishem) &
                    (imglist['BOUNDARY_VALID']==True) &
                    (imglist['SC_HI']==True)]
    data_cv = np.concatenate([selec['OPENCV_CENTERS'].tolist()])
    num_bounds_hi = np.sum(np.isfinite(data_cv))
        
    for tbctr in range(3):
        ax = axes[hemctr+2*tbctr]
        print(hemctr+2*tbctr)
        
        if not tbctr:
            clist = [myred,'k',myblue]
            name = ['poleward','center','equatorward']
            for iii in range(3):
                ax.plot(xdata, plot_LT[0,hemctr,iii,0,:], c=clist[iii], ls='-',
                        label= '{0} ({1:d})'.format(name[iii], num_bounds))
                ax.fill_between(xdata,
                                plot_LT[0,hemctr,iii,0,:]-plot_LT[0,hemctr,iii,1,:],
                                plot_LT[0,hemctr,iii,0,:]+plot_LT[0,hemctr,iii,1,:],
                                color=clist[iii],
                                alpha=0.2)
                
        elif tbctr==1:
            iii=1
            ax.plot(xdata, plot_LT_seas[0,hemctr,iii,0,:], c='g', ls='-',
                    label= 'pre-equinox ({0:d})'.format(num_bounds_pre))
            ax.plot(xdata, plot_LT_seas[1,hemctr,iii,0,:], c='darkorange', ls='-',
                    label= 'post-equinox ({0:d})'.format(num_bounds_post))
            ax.fill_between(xdata,
                            plot_LT_seas[0,hemctr,iii,0,:]-plot_LT_seas[0,hemctr,iii,1,:],
                            plot_LT_seas[0,hemctr,iii,0,:]+plot_LT_seas[0,hemctr,iii,1,:],
                            color='g',
                            alpha=0.2)
            ax.fill_between(xdata,
                            plot_LT_seas[1,hemctr,iii,0,:]-plot_LT_seas[1,hemctr,iii,1,:],
                            plot_LT_seas[1,hemctr,iii,0,:]+plot_LT_seas[1,hemctr,iii,1,:],
                            color='darkorange',
                            alpha=0.2)
        elif tbctr==2:
            iii=1
            ax.plot(xdata, plot_LT_sc[0,hemctr,iii,0,:], c='g', ls='-',
                    label= 'low solar activity ({0:d})'.format(num_bounds_lo))
            ax.plot(xdata, plot_LT_sc[1,hemctr,iii,0,:], c='darkorange', ls='-',
                    label= 'high solar activity ({0:d})'.format(num_bounds_hi))
            ax.fill_between(xdata,
                            plot_LT_sc[0,hemctr,iii,0,:]-plot_LT_sc[0,hemctr,iii,1,:],
                            plot_LT_sc[0,hemctr,iii,0,:]+plot_LT_sc[0,hemctr,iii,1,:],
                            color='g',
                            alpha=0.2)
            ax.fill_between(xdata,
                            plot_LT_sc[1,hemctr,iii,0,:]-plot_LT_sc[1,hemctr,iii,1,:],
                            plot_LT_sc[1,hemctr,iii,0,:]+plot_LT_sc[1,hemctr,iii,1,:],
                            color='darkorange',
                            alpha=0.2)
#        if thishem == 'North':
#            paper_x = [x_nic, x_nic, x_kin, x_kin]
#            paper_y = [y_nic_pol_n, y_nic_eq_n, y_kin_pol, y_kin_eq]
#            paper_c = ['g','blueviolet','g','blueviolet']
#        elif thishem == 'South':
#            paper_x = [x_nic, x_nic]
#            paper_y = [y_nic_pol_s, y_nic_eq_s]
#            paper_c = ['g','blueviolet']
#        for iii in range(len(paper_x)):
#            ax.plot(paper_x[iii], paper_y[iii], c=paper_c[iii], ls='--')
        # settings
        ax.grid('on')
        ax.set_xlim([0,2*np.pi])
        ax.set_xticks(np.arange(0,5)*np.pi/2)
        ax.set_xticklabels([0,6,12,18,24])
        if tbctr>1:
            ax.set_xlabel(r'LT (h)'.format(thishem[0]))
        if thishem=='North':
            exmax = 1/2*np.pi
            disp = 1
        else:
            exmax = 3/2*np.pi
            disp = -1
        ax.text(0.015,0.93,'({}) {}'.format(panels[hemctr+2*tbctr], thishem),
                transform=ax.transAxes, fontweight='bold', fontsize=14)
ax1.set_ylabel('Colatitude (deg)')
ax3.set_ylabel('Colatitude (deg)')
ax5.set_ylabel('Colatitude (deg)')
ax1.legend(loc=3)
ax2.legend(loc=4)
ax3.legend(loc=3)
ax4.legend(loc=4)
ax5.legend(loc=3)
ax6.legend(loc=4)
ax1.tick_params(direction='in',bottom=True,left=True,top=True,right=True,
                labelleft=True, labelright=False, labelbottom=False)
ax2.tick_params(direction='in',bottom=True,left=True,top=True,right=True,
                labelleft=False, labelright=False, labelbottom=False)
ax3.tick_params(direction='in',bottom=True,left=True,top=True,right=True,
                labelleft=True, labelright=False, labelbottom=False)
ax4.tick_params(direction='in',bottom=True,left=True,top=True,right=True,
                labelleft=False, labelright=False, labelbottom=False)
ax5.tick_params(direction='in',bottom=True,left=True,top=True,right=True,
                labelleft=True, labelright=False, labelbottom=True)
ax6.tick_params(direction='in',bottom=True,left=True,top=True,right=True,
                labelleft=False, labelright=False, labelbottom=True)

plt.savefig('{}/bounds_LT_cv.png'.format(mainsavepath),
            bbox_inches='tight', dpi=500)
plt.savefig('{}/bounds_LT_cv.pdf'.format(mainsavepath),
            bbox_inches='tight', dpi=500)
plt.show()
plt.close()

#=========================================
#print data to files, ready for TeX tables
for hhh in range(len(hems)):
    thishem = hems[hhh]
    with open('{}/LT_table_{}.txt'.format(mainsavepath, thishem), 'w') as file:
        for iii in range(int(np.shape(LT_med_cv)[-1]/2)):
            lonvalue = np.mean(loncenters[2*iii:2*iii+2])
            LTvalue = lonvalue/360*24
            LTstr = '{0:02d}:{1:02d}'.format(int(np.floor(LTvalue)),
                              int(np.round((LTvalue%1)*60)))
            print('{0:d} ({1}) & {2:0.1f} & {3:0.1f} & {4:0.1f} & {5:0.1f} & {6:0.1f} & {7:0.1f} \\\\'.format(
                    int(lonvalue),
                    LTstr,
                    np.mean(LT_med_cv[0,hhh,0,0,2*iii:2*(iii+1)]),
                    np.mean(LT_med_cv[0,hhh,0,1,2*iii:2*(iii+1)]),
                    np.mean(LT_med_cv[0,hhh,1,0,2*iii:2*(iii+1)]),
                    np.mean(LT_med_cv[0,hhh,1,1,2*iii:2*(iii+1)]),
                    np.mean(LT_med_cv[0,hhh,2,0,2*iii:2*(iii+1)]),
                    np.mean(LT_med_cv[0,hhh,2,1,2*iii:2*(iii+1)]),
                ), file=file)
            
with open('{}/LT_table_comb.txt'.format(mainsavepath, thishem), 'w') as file:
    
    for iii in range(int(np.shape(LT_med_cv)[-1]/2)):
        lonvalue = np.mean(loncenters[2*iii:2*iii+2])
        LTvalue = lonvalue/360*24
        LTstr = '{0:02d}:{1:02d}'.format(int(np.floor(LTvalue)),
                          int(np.round((LTvalue%1)*60)))
        str1 = '{0:d} ({1}) & {2:0.1f} & {3:0.1f} & {4:0.1f} & {5:0.1f} & {6:0.1f} & {7:0.1f} '.format(
                    int(lonvalue),
                    LTstr,
                    np.mean(LT_med_cv[0,0,0,0,2*iii:2*(iii+1)]),
                    np.mean(LT_med_cv[0,0,0,1,2*iii:2*(iii+1)]),
                    np.mean(LT_med_cv[0,0,1,0,2*iii:2*(iii+1)]),
                    np.mean(LT_med_cv[0,0,1,1,2*iii:2*(iii+1)]),
                    np.mean(LT_med_cv[0,0,2,0,2*iii:2*(iii+1)]),
                    np.mean(LT_med_cv[0,0,2,1,2*iii:2*(iii+1)]),
                )
        str2 = '& {0:0.1f} & {1:0.1f} & {2:0.1f} & {3:0.1f} & {4:0.1f} & {5:0.1f}\\\\'.format(
                    np.mean(LT_med_cv[0,1,0,0,2*iii:2*(iii+1)]),
                    np.mean(LT_med_cv[0,1,0,1,2*iii:2*(iii+1)]),
                    np.mean(LT_med_cv[0,1,1,0,2*iii:2*(iii+1)]),
                    np.mean(LT_med_cv[0,1,1,1,2*iii:2*(iii+1)]),
                    np.mean(LT_med_cv[0,1,2,0,2*iii:2*(iii+1)]),
                    np.mean(LT_med_cv[0,1,2,1,2*iii:2*(iii+1)]),
                )
        print(str1+str2, file=file)
        
with open('{}/LT_table_comb_long.txt'.format(mainsavepath, thishem), 'w') as file:
    s1 = 'Longitude & N_pol & N_pol_err & N_cen & N_cen_err & N_eqw & N_eqw_err'
    s2 = 'S_pol & S_pol_err & S_cen & S_cen_err & S_eqw & S_eqw_err'
    print(s1+s2, file=file)
    for iii in range(int(np.shape(LT_med_cv)[-1])):
        lonvalue = loncenters[iii]
        str1 = '{0:.1f} & {1:0.1f} & {2:0.1f} & {3:0.1f} & {4:0.1f} & {5:0.1f} & {6:0.1f} '.format(
                    lonvalue,
                    np.mean(LT_med_cv[0,0,0,0,iii]),
                    np.mean(LT_med_cv[0,0,0,1,iii]),
                    np.mean(LT_med_cv[0,0,1,0,iii]),
                    np.mean(LT_med_cv[0,0,1,1,iii]),
                    np.mean(LT_med_cv[0,0,2,0,iii]),
                    np.mean(LT_med_cv[0,0,2,1,iii]),
                )
        str2 = '& {0:0.1f} & {1:0.1f} & {2:0.1f} & {3:0.1f} & {4:0.1f} & {5:0.1f}'.format(
                    np.mean(LT_med_cv[0,1,0,0,iii]),
                    np.mean(LT_med_cv[0,1,0,1,iii]),
                    np.mean(LT_med_cv[0,1,1,0,iii]),
                    np.mean(LT_med_cv[0,1,1,1,iii]),
                    np.mean(LT_med_cv[0,1,2,0,iii]),
                    np.mean(LT_med_cv[0,1,2,1,iii]),
                )
        print(str1+str2, file=file)
        
with open('{}/PPO_table_comb.txt'.format(mainsavepath, thishem), 'w') as file:
    print('Psi & N_pri_offset & N_pri_err & N_sec_offset & N_sec_err & S_pri_offset & S_pri_err & S_sec_offset & S_sec_err', file=file)
    for iii in range(int(np.shape(LT_med_cv)[-1])):
        lonvalue = loncenters[iii]
        str1 = '{0:.1f} & {1:0.3f} & {2:0.3f} & {3:0.3f} & {4:0.3f} '.format(
                    lonvalue,
                    np.mean(comp_data_cv_sc[0,0,0,0,iii]),
                    np.mean(comp_data_cv_sc[0,0,0,1,iii]),
                    np.mean(comp_data_cv_sc[0,0,1,0,iii]),
                    np.mean(comp_data_cv_sc[0,0,1,1,iii]),
                )
        str2 = '& {0:0.3f} & {1:0.3f} & {2:0.3f} & {3:0.3f}'.format(
                    np.mean(comp_data_cv_sc[1,0,0,0,iii]),
                    np.mean(comp_data_cv_sc[1,0,0,1,iii]),
                    np.mean(comp_data_cv_sc[1,0,1,0,iii]),
                    np.mean(comp_data_cv_sc[1,0,1,1,iii]),
                )
        print(str1+str2, file=file)

