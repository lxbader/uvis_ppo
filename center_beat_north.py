# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cubehelix
import matplotlib
import matplotlib.colors as mcolors
from matplotlib.backends.backend_pdf import PdfPages
from matplotlib.collections import LineCollection
import matplotlib.pyplot as plt
import numpy as np
import os
import scipy.optimize as sopt
import time
import time_conversions as tc
import uvisdb

myblue='royalblue'
myred='crimson'

udb = uvisdb.UVISDB()
imglist = udb.currentDF

def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = mcolors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)
cmap_UV_short = truncate_colormap(cmap_UV, 0.3, 0.9, 100)
cmap_SPEC = cubehelix.cmap(reverse=False, start=1.8, rot=1, gamma=1.5)
cmap_SPEC_short = truncate_colormap(cmap_SPEC, 0.25, 0.75, 100)

imglist['IMGTIME'] = imglist[['ET_START','ET_STOP']].mean(axis=1)
# determine beat phase for all images        
# calculate PPO beat phase
imglist['PPO_SminN'] = (imglist['PPO_PHASE_S'] - imglist['PPO_PHASE_N']) % (2*np.pi)
imglist['PPO_BEAT_PHASE'] = 0
# in phase
imglist['PPO_BEAT_PHASE'][(imglist['PPO_SminN']<=1/4*np.pi) |
                            (imglist['PPO_SminN']>7/4*np.pi)] = 1
# S leading N
imglist['PPO_BEAT_PHASE'][(imglist['PPO_SminN']>1/4*np.pi) &
                            (imglist['PPO_SminN']<=3/4*np.pi)] = 2
# antiphase
imglist['PPO_BEAT_PHASE'][(imglist['PPO_SminN']>3/4*np.pi) &
                            (imglist['PPO_SminN']<=5/4*np.pi)] = 3
# S lagging N
imglist['PPO_BEAT_PHASE'][(imglist['PPO_SminN']>5/4*np.pi) &
                            (imglist['PPO_SminN']<=7/4*np.pi)] = 4
       
beatphase = ['00 all','01 in phase','02 S leading N','03 in antiphase','04 S lagging N']
cmaps = [plt.cm.Greys,plt.cm.Blues,plt.cm.Reds,plt.cm.Greens,plt.cm.Oranges]
colors = ['k','b','r','g','darkorange']

# which images correspond to which subinterval
datefmt = '%Y-%jT%H-%M-%S'
subintervnames = ['SUBINT_all', 'SUBINT_outside_coalescence', 'SUBINT_during_coalescence']
for iii in range(len(subintervnames)):
    current = subintervnames[iii]
    imglist[current] = 0
    if current == 'SUBINT_all':
        imglist[current] = 1
    elif current == 'SUBINT_outside_coalescence':
        imglist[current][(imglist['IMGTIME'] <= tc.str2et('2013-181T00-00-00', datefmt)) | 
                         (imglist['IMGTIME'] >= tc.str2et('2014-181T00-00-00', datefmt))] = 1
    elif current == 'SUBINT_during_coalescence':
        imglist[current][(imglist['IMGTIME'] > tc.str2et('2013-181T00-00-00', datefmt)) & 
                         (imglist['IMGTIME'] < tc.str2et('2014-181T00-00-00', datefmt))] = 1
               
# how many boundary points are available
imglist['OPENCV_CENTERS_PTS'] = np.array([np.sum(np.isfinite(imglist.loc[iii, 'OPENCV_CENTERS'])) for iii in imglist.index])
boundlons = np.linspace(0,360,num=len(imglist.loc[0,'OPENCV_CENTERS'])+1)
boundlons = boundlons[:-1] + np.diff(boundlons)/2
boundquads = np.zeros_like(boundlons)
boundquads[np.where((boundlons>45) & (boundlons<135))] = 0
boundquads[np.where((boundlons>135) & (boundlons<225))] = 1
boundquads[np.where((boundlons>225) & (boundlons<315))] = 2
boundquads[np.where((boundlons>315) | (boundlons<45))] = 3
imglist['OPENCV_CENTERS_QUADS'] = 0
for iii in imglist.index:
    quads = 0
    for jjj in np.unique(boundquads):
        quads += np.sum(np.isfinite(imglist.loc[iii,'OPENCV_CENTERS'][np.where(boundquads==jjj)])) > 3
    imglist.at[iii,'OPENCV_CENTERS_QUADS'] = quads

# mark valid circle fits
imglist['CIRCLE_FIT_VALID'] = True
imglist.at[imglist['POS_KRTP_R'] > 30,'CIRCLE_FIT_VALID'] = False
imglist.at[imglist['EXP'] < 500,'CIRCLE_FIT_VALID'] = False
imglist.at[imglist['OPENCV_CENTERS_QUADS'] < 4,'CIRCLE_FIT_VALID'] = False
imglist.at[imglist['OPENCV_CENTERS_PTS'] < 36,'CIRCLE_FIT_VALID'] = False
imglist.at[np.abs(imglist['POS_KRTP_LAT']) < 30,'CIRCLE_FIT_VALID'] = False

imglist['CENT_X'] = imglist['CIRCLE_X']
imglist['CENT_Y'] = imglist['CIRCLE_Y']
imglist['CENT_STAT'] = imglist['CIRCLE_STAT']

#imglist['CENT_X'] = imglist['ELLIPSE_X']
#imglist['CENT_Y'] = imglist['ELLIPSE_Y']
#imglist['CENT_STAT'] = imglist['ELLIPSE_STAT']

imglist.at[np.isnan(imglist['CENT_X']),'CIRCLE_FIT_VALID'] = False
imglist.at[imglist['CENT_STAT']>2,'CIRCLE_FIT_VALID'] = False


imglist['FILENAME'] = [imglist.loc[iii,'FILEPATH'].strip('.fits').split('/')[-1] for iii in imglist.index]

imglist['CV_CIRC_VALID'] = False

# 2007
imglist.at[(imglist['YEAR']==2007)&(imglist['DOY']==96),'CV_CIRC_VALID'] = True

# 2008
imglist.at[(imglist['YEAR']==2008)&(imglist['DOY']>=0)&(imglist['DOY']<=100),'CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2008_002T19_36_47','CV_CIRC_VALID'] = False
imglist.at[imglist['FILENAME'] =='2008_002T20_02_46','CV_CIRC_VALID'] = False
imglist.at[imglist['FILENAME'] =='2008_037T11_22_37','CV_CIRC_VALID'] = False
imglist.at[(imglist['YEAR']==2008)&(imglist['DOY']==197),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2008)&(imglist['DOY']==37),'CV_CIRC_VALID'] = False

imglist.at[(imglist['YEAR']==2008)&(imglist['DOY']>=109)&(imglist['DOY']<=129),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2008)&(imglist['DOY']==195),'CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2008_197T04_21_01','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2008_197T04_48_29','CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2008)&(imglist['DOY']>=240)&(imglist['DOY']<=255),'CV_CIRC_VALID'] = True

imglist.at[(imglist['YEAR']==2008)&(imglist['DOY']>=224)&(imglist['DOY']<=255),'CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2008_238T22_29_51','CV_CIRC_VALID'] = False
imglist.at[imglist['FILENAME'] =='2008_238T23_01_19','CV_CIRC_VALID'] = False
imglist.at[imglist['FILENAME'] =='2008_239T07_36_01','CV_CIRC_VALID'] = False

imglist.at[(imglist['YEAR']==2008)&(imglist['DOY']>=304)&(imglist['DOY']<=335),'CV_CIRC_VALID'] = True


# 2009
imglist.at[(imglist['YEAR']==2009)&(imglist['DOY']>=21)&(imglist['DOY']<=23),'CV_CIRC_VALID'] = True

# 2012
imglist.at[(imglist['YEAR']==2012)&(imglist['DOY']==343),'CV_CIRC_VALID'] = True

# 2013
imglist.at[(imglist['YEAR']==2013)&(imglist['DOY']>=3)&(imglist['DOY']<=70),'CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2013_015T06_52_08','CV_CIRC_VALID'] = False
imglist.at[imglist['FILENAME'] =='2013_081T22_18_08','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2013_082T00_26_39','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2013_082T02_27_09','CV_CIRC_VALID'] = True

imglist.at[(imglist['YEAR']==2013)&(imglist['DOY']>=109)&(imglist['DOY']<=139),'CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2013_140T00_50_55','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2013_140T02_15_56','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2013_140T03_40_57','CV_CIRC_VALID'] = True

imglist.at[imglist['FILENAME'] =='2013_140T22_53_18','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2013_140T23_56_54','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2013_141T01_00_22','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2013_141T16_20_11','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2013_141T17_36_47','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2013_176T17_59_49','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2013_176T18_22_21','CV_CIRC_VALID'] = True

imglist.at[(imglist['YEAR']==2013)&(imglist['DOY']>=228)&(imglist['DOY']<=228),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2013)&(imglist['DOY']>=251)&(imglist['DOY']<=251),'CV_CIRC_VALID'] = True

imglist.at[(imglist['YEAR']==2013)&(imglist['DOY']>=330)&(imglist['DOY']<=362),'CV_CIRC_VALID'] = True

# 2014
imglist.at[(imglist['YEAR']==2014)&(imglist['DOY']==31),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2014)&(imglist['DOY']==99),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2014)&(imglist['DOY']==135),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2014)&(imglist['DOY']==166),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2014)&(imglist['DOY']==167),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2014)&(imglist['DOY']==230),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2014)&(imglist['DOY']==294),'CV_CIRC_VALID'] = True

# 2016
imglist.at[imglist['FILENAME'] =='2016_157T18_00_41','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2016_157T18_43_32','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2016_157T19_26_28','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2016_157T20_09_24','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2016_157T20_52_20','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2016_157T21_35_16','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2016_157T22_18_12','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2016_157T23_01_08','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2016_157T23_44_04','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2016_158T00_27_00','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2016_158T01_09_56','CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2016)&(imglist['DOY']==181),'CV_CIRC_VALID'] = True

imglist.at[imglist['FILENAME'] =='2016_204T21_16_40','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2016_204T21_51_04','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2016_204T22_25_20','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2016_204T22_59_36','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2016_204T23_33_52','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2016_205T00_42_24','CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2016)&(imglist['DOY']==278),'CV_CIRC_VALID'] = True

imglist.at[(imglist['YEAR']==2016)&(imglist['DOY']>=229)&(imglist['DOY']<=230),'CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2016_230T14_44_35','CV_CIRC_VALID'] = False
imglist.at[imglist['FILENAME'] =='2016_230T14_50_51','CV_CIRC_VALID'] = False
imglist.at[imglist['FILENAME'] =='2016_230T20_39_30','CV_CIRC_VALID'] = False
imglist.at[imglist['FILENAME'] =='2016_231T00_12_45','CV_CIRC_VALID'] = True

imglist.at[(imglist['YEAR']==2016)&(imglist['DOY']>=250)&(imglist['DOY']<=251),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2016)&(imglist['DOY']>=275)&(imglist['DOY']<=277),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2016)&(imglist['DOY']>=303)&(imglist['DOY']<=315),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2016)&(imglist['DOY']>=322)&(imglist['DOY']<=360),'CV_CIRC_VALID'] = True

# 2017
imglist.at[imglist['FILENAME'] =='2017_017T06_30_11','CV_CIRC_VALID'] = True

imglist.at[(imglist['YEAR']==2017)&(imglist['DOY']>=23)&(imglist['DOY']<=30),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2017)&(imglist['DOY']>=58)&(imglist['DOY']<=58),'CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2017_067T12_31_27','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2017_067T13_08_07','CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2017)&(imglist['DOY']==79)&(imglist['POS_KRTP_LAT']>32.3),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2017)&(imglist['DOY']>=80)&(imglist['DOY']<=87),'CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2017_080T02_20_16','CV_CIRC_VALID'] = False

imglist.at[imglist['FILENAME'] =='2017_102T08_21_12','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2017_167T14_36_25','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2017_167T16_25_36','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2017_167T19_04_16','CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2017)&(imglist['DOY']==207),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2017)&(imglist['DOY']>=212)&(imglist['DOY']<=219),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2017)&(imglist['DOY']>=232)&(imglist['DOY']<=232),'CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2017_239T07_03_49','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2017_245T18_00_54','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2017_252T04_53_00','CV_CIRC_VALID'] = True
imglist.at[imglist['FILENAME'] =='2017_252T05_45_16','CV_CIRC_VALID'] = True


# maybe
imglist.at[(imglist['YEAR']==2013)&(imglist['DOY']>=78)&(imglist['DOY']<=78),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2013)&(imglist['DOY']>=174)&(imglist['DOY']<=174),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2014)&(imglist['DOY']>=294)&(imglist['DOY']<=294),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2014)&(imglist['DOY']>=342)&(imglist['DOY']<=343)&(imglist['POS_KRTP_LAT']>25),'CV_CIRC_VALID'] = True
imglist.at[(imglist['YEAR']==2016)&(imglist['DOY']>=125)&(imglist['DOY']<=126),'CV_CIRC_VALID'] = True

imglist.at[np.isnan(imglist['CENT_X']),'CV_CIRC_VALID'] = False
imglist.at[np.sqrt(imglist['CENT_STAT'])>1,'CV_CIRC_VALID'] = False

imglist['BOUNDARY_VALID'] = True
imglist.at[imglist['POS_KRTP_R'] > 40,'BOUNDARY_VALID'] = False
#imglist.at[imglist['EXP'] < 500,'BOUNDARY_VALID'] = False
# exclude a bunch of weird images
imglist.at[(imglist['YEAR']==2013)&(imglist['DOY']>=233)&(imglist['DOY']<=239),'BOUNDARY_VALID'] = False
imglist.at[(imglist['YEAR']==2017)&(imglist['DOY']>=196)&(imglist['DOY']<=197),'BOUNDARY_VALID'] = False
imglist.at[(imglist['YEAR']==2017)&(imglist['DOY']>=236)&(imglist['DOY']<=237),'BOUNDARY_VALID'] = False
imglist.at[(imglist['YEAR']==2017)&(imglist['DOY']>=257)&(imglist['DOY']<=257),'BOUNDARY_VALID'] = False



cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)
cmap_SPEC = cubehelix.cmap(reverse=False, start=1.8, rot=1, gamma=1.5)

# define trigonometric function with several free parameters
def freeTrig(x,ampl,phaseshift,yoffset):
    return ampl*np.sin(x+phaseshift)+yoffset

#===================================================
# fitting and plotting the ellipse fit center motion

# path for saving plots
mainsavepath = '%s/Plots/PPO_ellipse_beat/%s' % (boxpath, time.strftime('%Y%m%d%H%M%S'))
if not os.path.exists(mainsavepath):
    os.makedirs(mainsavepath)
    
hemispheres = ['North', 'South']
refsys = ['PPO_PHASE_N','PPO_PHASE_S']

phisp = np.linspace(0,2*np.pi,num=200)
BINS = 36

phispace = np.linspace(0,360,num=361)
xplot = np.zeros((2,len(phispace)))
yplot = np.zeros((2,len(phispace)))
xopt = np.zeros((2,3))
yopt = np.zeros((2,3))
    
with PdfPages('{}/sine_fits.pdf'.format(mainsavepath)) as pdf:
    for hctr in range(len(hemispheres)):
    #for hctr in [0]:
        thishem = hemispheres[hctr]
        
        for rctr in range(len(refsys)):
            thisref = refsys[rctr]
            if thisref[-1] != thishem[0]:
                continue
        
    #        for sctr in range(len(subintervnames)):
            for sctr in [1]:
                thissub = subintervnames[sctr]
                
                savepath = '{}/{}/{}/{}'.format(mainsavepath,thishem,thisref,thissub)
                if not os.path.exists(savepath):
                    os.makedirs(savepath)
                
                # storage for fit parameters [beat phase, x/y, opt]
                opt_storage = np.zeros((len(beatphase),2,3))
                std_storage = np.zeros((len(beatphase),2,3))
                num_storage = np.zeros((len(beatphase)), dtype=np.uint16)
                
                for bctr in range(len(beatphase)):
                    thisbeat = beatphase[bctr]
                    
                    df_selec = imglist[(imglist[thissub] == 1) &
                                       (imglist['PPO_BEAT_PHASE']>=0 if bctr==0
                                        else imglist['PPO_BEAT_PHASE']==bctr) &
                                       (imglist['HEMISPHERE'] == thishem) &
                                       (imglist['CV_CIRC_VALID'] == True) &
                                       (np.isfinite(imglist[thisref]))]
                    
                    # invert axes to fit to previous Nichols papers
                    refangle = (df_selec[thisref]+np.pi) % (2*np.pi)
                    xdata = -df_selec['CENT_X']
                    ydata = -df_selec['CENT_Y']
                    
                    # set up figure
                    fig = plt.figure()
                    fig.set_size_inches(10,10)
                    ax1 = plt.subplot(211)
                    ax2 = plt.subplot(212, sharex=ax1)
                    plt.subplots_adjust(hspace=0)
                    
                    for axctr in range(2):
                        ax = ax1 if axctr==0 else ax2
                        data = xdata if axctr==0 else ydata
                        
                        # bin data in phi bins and take median before fitting
                        tmp = np.digitize(refangle, np.linspace(0,2*np.pi,num=BINS+1))-1
                        data_med = np.array([np.median(data.iloc[np.where(tmp==iii)[0]])
                                             for iii in range(np.min(tmp), np.max(tmp)+1)])
                        bins_med = np.linspace(0,2*np.pi,num=BINS+1)
                        bins_med = bins_med[:-1] + np.diff(bins_med)/2
                        tmpval = np.where(np.isfinite(data_med))[0]
                        bins_med = bins_med[tmpval]
                        data_med = data_med[tmpval]
                        
                        # median boxfilter data
                        windowsize = np.radians(22.5)
                        bins_med_new = np.linspace(0,2*np.pi,num=361)
                        data_med_new = np.zeros_like(bins_med_new)
                        for iii in range(len(bins_med_new)):
                            upper = bins_med_new[iii] + windowsize
                            lower = bins_med_new[iii] - windowsize
                            if upper>2*np.pi or lower<0:
                                tmpval = np.where((refangle<(upper%(2*np.pi))) | (refangle>(lower%(2*np.pi))))[0]
                            else:
                                tmpval = np.where((refangle<(upper%(2*np.pi))) & (refangle>(lower%(2*np.pi))))[0]
                            data_med_new[iii] = np.nanmean(data.iloc[tmpval])
                        
                        if not np.any(data):
                            continue
                        
                        # plot data
                        ax.errorbar(refangle, data, yerr=df_selec['CENT_STAT'].apply(np.sqrt),
                                     fmt='x', color=colors[bctr],
                                     linewidth=0.5, label=None)
                        
                        if len(refangle) < 10:
                            continue
                        
                        # fit data
                        opt, cov = sopt.curve_fit(freeTrig, refangle, data,
                                                  sigma=np.sqrt(df_selec['CENT_STAT']),
                                                  absolute_sigma=True,
                                                  method='trf',
                                                  p0=(0.5,0,-1),
                                                  loss='soft_l1',
                                                  )
                        if ax == ax1 and thishem[0]==thisref[-1] and bctr==0 and sctr==1:
                            xplot[hctr,:] = freeTrig(np.radians(phispace),opt[0],opt[1],opt[2])
                            xopt[hctr,:] = opt
                        elif ax == ax2 and thishem[0]==thisref[-1] and bctr==0 and sctr==1:
                            yplot[hctr,:] = freeTrig(np.radians(phispace),opt[0],opt[1],opt[2])
                            yopt[hctr,:] = opt
    
                        # fix negative amplitudes
                        if opt[0] < 0:
                            opt[0] = np.abs(opt[0])
                            shift = opt[1]+np.pi
                            if shift > np.pi:
                                shift = shift-2*np.pi
                            opt[1] = shift
                        opt_storage[bctr,axctr,:] = opt
                        std_storage[bctr,axctr,:] = np.sqrt(np.diag(cov))
                        num_storage[bctr] = len(refangle)
                        # plot fit
                        ax.plot(phisp, freeTrig(phisp,*opt), c=colors[bctr],
                                label=r'$%.1f*\mathrm{cos}(\Phi_\mathrm{%s}%+.1f)%+.1f$' % (opt[0],
                                                 'N' if thishem=='North' else 'S',
                                                 (opt[1]*180/np.pi-90)%360-360, opt[2]), zorder=5)
                        
                        # plot Nichols fits
                        if not bctr:
                            if thishem=='North' and thisref=='PPO_PHASE_N':                        
                                if ax==ax1:
                                    ax.plot(phisp, freeTrig(phisp,0.5,66/180*np.pi,-1.3), color=myred, label=r'$0.5*\mathrm{cos}(\Phi_\mathrm{N}-24)-1.3$ (Nichols 2016)')
                                else:
                                    ax.plot(phisp, freeTrig(phisp,1.6,-41/180*np.pi,-0.9), color=myred, label=r'$1.6*\mathrm{cos}(\Phi_\mathrm{N}-131)-0.9$ (Nichols 2016)')
                            if thishem=='South' and thisref=='PPO_PHASE_S':                        
                                if ax==ax1:
                                    ax.plot(phisp, freeTrig(phisp,1.8,-357/180*np.pi,-1.7), color=myred, label=r'$1.8*\mathrm{cos}(\Phi_\mathrm{S}-87)-1.7$ (Nichols 2008)')
                                else:
                                    ax.plot(phisp, freeTrig(phisp,1.3,202/180*np.pi,-1.4), color=myred, label=r'$1.3*\mathrm{cos}(\Phi_\mathrm{S}-248)-1.4$ (Nichols 2008)')
                        
                        # mark phi angle with maximum displacement and its std
                        vmax = (np.pi/2-opt[1]) % (2*np.pi)
                        ax.axvline(vmax, color=colors[bctr], linestyle='--')
                        std = np.sqrt(np.diag(cov))
                        ylim = np.array(ax.get_ylim())
                        
                        # general setup
                        ax.set_xlim([0,2*np.pi])
                        ax.set_xticks([0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi])
                        ax.set_xticklabels([0,90,180,270,360])
                        ax.set_xlabel(r'$\Phi_\mathrm{{{0}}}$ (deg)'.format('N' if thisref=='PPO_PHASE_N' else 'S'))
                        if not axctr:
                            plt.setp(ax.get_xticklabels(), visible=False)
                        ax.set_ylim(ylim)
                        ax.set_ylim([-4,3])
                        ax.set_ylabel('x-displacement (deg) (midnight/noon -/+)' if axctr==0 else
                                      'y-displacement (deg) (dawn/dusk -/+)',
                                      rotation = 270 if axctr else 90,
                                      labelpad = 10 if axctr else 0)
                        ax.legend(loc=4)
                        ax.grid(alpha=0.3, c='k')
                        if axctr:
                            ax.yaxis.set_label_position('right')
                            ax.yaxis.tick_right()
                    ax1.set_title('{}ern hemisphere, {} ({})'.format(thishem, thisbeat[3:], len(xdata)))
                    
                    pdf.savefig()

                    ax1.text(0.01,0.92,'(a)' if thishem=='North' else '(c)',
                                transform=ax1.transAxes, fontsize=20, fontweight='bold')
                    ax2.text(0.01,0.92,'(b)' if thishem=='North' else '(d)',
                                transform=ax2.transAxes, fontsize=20, fontweight='bold')
                    
                    plt.savefig('{}/disp_{}_{}_{}.png'.format(savepath,thisbeat.replace(' ','_'), thishem, sctr),
                        bbox_inches='tight', dpi=500)
                    plt.savefig('{}/disp_{}_{}_{}.pdf'.format(savepath,thisbeat.replace(' ','_'), thishem, sctr),
                        bbox_inches='tight', dpi=500)
                    
                    plt.show()
                    plt.close()
                    
                if hctr==0 and sctr==1:
                    opt_storage_n = np.copy(opt_storage)
                    std_storage_n = np.copy(std_storage)
                    num_storage_n = np.copy(num_storage)
                elif hctr==1 and sctr==1:
                    opt_storage_s = np.copy(opt_storage)
                    std_storage_s = np.copy(std_storage)
                    num_storage_s = np.copy(num_storage)

# make ellipse plots here
fig = plt.figure()
fig.set_size_inches(12,7)
phimarkers = np.arange(0,2*np.pi,1/4*np.pi)
ax1 = fig.add_subplot(121, aspect='equal')
ax2 = fig.add_subplot(122, aspect='equal')
axes = [ax1,ax2]
phisp = np.linspace(0,2*np.pi,num=150)
for hctr in range(2):
    ax = axes[hctr]
    opt_storage = opt_storage_n if not hctr else opt_storage_s
    num_storage = num_storage_n if not hctr else num_storage_s
    for bctr in range(len(beatphase)):
        if not (np.all(opt_storage[bctr,1,:]) and np.all(opt_storage[bctr,0,:])):
            continue
        ax.scatter(freeTrig(phisp, *opt_storage[bctr,1,:]),
                   -freeTrig(phisp, *opt_storage[bctr,0,:]),
                   c=np.arange(len(phisp)), cmap=cmaps[bctr],
                   label='{} ({})'.format(beatphase[bctr][3:], num_storage[bctr]),
                   s=10)
#        ax.scatter(freeTrig(phimarkers, *opt_storage[bctr,1,:]),
#                   -freeTrig(phimarkers, *opt_storage[bctr,0,:]),
#                   edgecolor='none',
#                   color='k',
#                   marker='x',
#                   s=10)
        ax.scatter(opt_storage[bctr,1,2],-opt_storage[bctr,0,2], marker='+',
                   edgecolor='none', s=200, linewidth=1.5, color=cmaps[bctr](.8), zorder=5)
    ax.grid()
    ax.set_xlabel('dawn/dusk (-/+) displacement (deg)')
    ax.set_ylabel('noon/midnight (-/+) displacement (deg)')
    tmp = ax.legend(loc = 3 if hctr else 4)
    iii = 0
    for bctr in range(len(beatphase)):
        if not (np.all(opt_storage[bctr,1,:]) and np.all(opt_storage[bctr,0,:])):
            continue
        tmp.legendHandles[iii].set_color(cmaps[bctr](.6))
        iii += 1
    ax.axvline(0, c='k', ls='--')
    ax.axhline(0, c='k', ls='--')
    ax.text(0.02, 0.93, '({}) {}'.format('b' if hctr else 'a', 'South' if hctr else 'North'),
            transform=ax.transAxes,
            fontsize=15, fontweight='bold')
    
plt.savefig('{}/ellipses_beat.png'.format(mainsavepath),
        bbox_inches='tight', dpi=500)
plt.savefig('{}/ellipses_beat.pdf'.format(mainsavepath),
        bbox_inches='tight', dpi=500)
plt.show()
plt.close()
            
            
#=============================================
# compare N and S ellipses
            
def add_subplot_axes(ax,rect):
    fig = plt.gcf()
    box = ax.get_position()
    width = box.width
    height = box.height
    inax_position  = ax.transAxes.transform(rect[0:2])
    transFigure = fig.transFigure.inverted()
    infig_position = transFigure.transform(inax_position)    
    x = infig_position[0]
    y = infig_position[1]
    width *= rect[2]
    height *= rect[3]  # <= Typo was here
    subax = fig.add_axes([x,y,width,height], projection='polar')
    x_labelsize = subax.get_xticklabels()[0].get_size()
    y_labelsize = subax.get_yticklabels()[0].get_size()
    x_labelsize *= rect[2]**0.5
    y_labelsize *= rect[3]**0.5
    subax.xaxis.set_tick_params(labelsize=x_labelsize)
    subax.yaxis.set_tick_params(labelsize=y_labelsize)
    return subax


fig = plt.figure()
fig.set_size_inches(9,6)
#fig.set_figwidth(12)
ax1 = fig.add_subplot(121, aspect='equal')
ax2 = fig.add_subplot(122, aspect='equal', sharex=ax1, sharey=ax1)

for axctr in range(2):
    ax = [ax1,ax2][axctr]
    hctr = [0,1][axctr]
    rctr = [0,1][axctr]
    
    t = np.linspace(1,0,num=len(xplot[0,:]))
    x = yplot[hctr,:]
    y = -xplot[hctr,:]
    points = np.array([x, y]).T.reshape(-1, 1, 2)
    segments = np.concatenate([points[:-1], points[1:]], axis=1)
    
    lc = LineCollection(segments, cmap=cmap_UV,
                        norm=plt.Normalize(0, 1))
    lc.set_array(t)
    lc.set_linewidth(7)
    ax.add_collection(lc)
    
    ax.plot(x,y,c='none')

    ax.scatter(yopt[hctr,2], -xopt[hctr,2], marker='+',
               s=100, linewidth=0.5, color='k')
    
    angles = np.arctan2(np.diff(y), np.diff(x))*180/np.pi
    angles = np.append(angles, [angles[-1]])
    
    # mark certain PPO angles
    ppoangles = [0,45,90,135,180,225,270,315]
    for iii in range(len(ppoangles)):
        amin = np.argmin(np.abs(ppoangles[iii]-phispace))
        t = matplotlib.markers.MarkerStyle(marker='|')
        t._transform = t.get_transform().rotate_deg(angles[amin])
        ax.scatter(x[amin], y[amin], marker=t, linewidth=1.5,
                   s=200, c='k', zorder=5)
        textangle = (angles[amin]-90)/180*np.pi
        [xoffs, yoffs] = np.array([np.cos(textangle), np.sin(textangle)])*0.2
        if iii==0:
            tmptxt = '$\Phi_\mathrm{%s}=%d^\mathrm{o}$' % ('N' if not axctr else 'S', ppoangles[iii]) 
        else:
            tmptxt = '$%d^\mathrm{o}$' % (ppoangles[iii])
        ax.annotate(tmptxt,
                    xy=(x[amin],y[amin]), xytext=(x[amin]+xoffs,y[amin]+yoffs),
                    ha='center', va='center')
        
    # calculate Psi range
    xdiff = xplot[hctr,:]-xopt[hctr,2]
    ydiff = yplot[hctr,:]-yopt[hctr,2]
    tmpangle = np.arctan2(ydiff,xdiff)*180/np.pi % 360
    psispace = (phispace-tmpangle) % 360
    psispace[np.where(psispace>300)] -= 360
    ax.text(0.50,0.02, 'Max displacement direction: $\Psi_\mathrm{%s}=%d$ to $%d^\mathrm{o}$' % (
            'N' if not axctr else 'S',
            int(np.min(psispace) if np.min(psispace)>-180 else np.min(psispace)+360),
            int(np.max(psispace))),
        ha='center', va='bottom',
        transform=ax.transAxes,
        bbox={'boxstyle':'round', 'facecolor':'0.7', 'pad':0.5})
    
    ax.axvline(x=0, ymin=0, ymax=1, c='k', lw=1, ls='--')
    ax.axhline(y=0, xmin=0, xmax=1, c='k', lw=1, ls='--')
    ax.grid()
#    ax.set_title('North' if not axctr else 'South')
    
    ax.set_xlim([-2.5,1])
    ax.set_ylim([-0.5,3.5])
    
    ax.set_xlabel('Dawn - dusk colatitude (deg)')
    
    ax.text(0.02,0.95,'({}) {}'.format('b' if axctr else 'a', 'South' if axctr else 'North'), transform=ax.transAxes, fontweight='bold',
            fontsize=13)
    
    
ax1.set_ylabel('Noon - midnight colatitude (deg)')
plt.setp(ax2.get_yticklabels(), visible=False)
plt.subplots_adjust(wspace=0.05)

ax3 = add_subplot_axes(ax1, [0.25,0.48,0.5,0.5])
ax3.set_theta_zero_location('S')
ax3.set_xticks([0,np.pi/2,np.pi,3*np.pi/2])
ax3.set_xticklabels(['12 LT', '18 LT', '0 LT', '6 LT'], fontsize=9, fontweight='bold')
ctr = 0
for tick in ax3.get_xaxis().get_major_ticks():
    if not ctr % 2:
        tick.set_pad(-4.)
    else:
        tick.set_pad(5)
    ctr += 1
ax3.set_yticks([])
ax3.set_ylim([0,1])
ppoangles = [45,90,135,180,225,270,315]
offsets = [0,0,-2,-2,1,0,+2]
for ctr in range(len(ppoangles)):
    iii = ppoangles[ctr]
    width = 25
    xs = np.linspace(iii-width/2, iii+width/2, num=20)*np.pi/180
    ax3.fill_between(xs,0,1,color=cmap_UV(1-iii/360))
    pos = (iii+offsets[ctr])/180*np.pi 
    ax3.text(pos,0.65,'$%d^\mathrm{o}$' % (iii),
             ha='center', va='center',
             rotation=iii-90 if iii-90<=90 else iii+90,
             color='k' if iii<180 else 'w')
ax3.grid('off')

ax3.text(359*np.pi/180,0.65,'$\Phi=0^\mathrm{o}$',
             ha='center', va='center',
             rotation=-90, color='k')
    
xs1 = np.linspace(360-width/2,360, num=20)*np.pi/180
xs2 = np.linspace(0, width/2, num=20)*np.pi/180
ax3.fill_between(xs1,0.95,1, color='k')
ax3.fill_between(xs2,0.95,1, color='k')

xs1 = np.linspace(360-width/2,362-width/2, num=20)*np.pi/180
xs2 = np.linspace(width/2-2, width/2, num=20)*np.pi/180
ax3.fill_between(xs1,0,1, color='k')
ax3.fill_between(xs2,0,1, color='k')

    
plt.savefig('%s/ellipses.png' % (mainsavepath),
                bbox_inches='tight', dpi=500)
plt.savefig('%s/ellipses.pdf' % (mainsavepath),
                bbox_inches='tight', dpi=500)
plt.show()
plt.close()
    
    
    
    
#%%       
            
#===============================================
# plot year histograms
fig = plt.figure()
fig.set_size_inches(15,5)
ax1 = fig.add_subplot(121)
ax2 = fig.add_subplot(122, sharey=ax1)
axes = [ax1,ax2]
subf = ['(a)','(b)']
hems =['North','South']
for hemctr in range(len(hems)):
    thishem = hems[hemctr]   
    ax = axes[hemctr]
    
    years = np.arange(2007,2018)
    nums_bounds = np.full(np.shape(years), np.nan)
    nums_circs = np.full(np.shape(years), np.nan)
    for yyy in range(len(years)):
        nums_bounds[yyy] = len(imglist[
                (imglist['YEAR'] == years[yyy]) &
                (imglist['HEMISPHERE'] == thishem) &
                (imglist['SUBINT_outside_coalescence']) &
                (imglist['BOUNDARY_VALID'])
                ])
        nums_circs[yyy] = len(imglist[
                    (imglist['YEAR'] == years[yyy]) &
                    (imglist['HEMISPHERE'] == thishem) &
                    (imglist['SUBINT_outside_coalescence']) &
                    (imglist['CV_CIRC_VALID'])
                    ])
    print(thishem)
    print(np.sum(nums_bounds))
    print(np.sum(nums_circs))
    barlist = ax.bar(years-0.2,nums_bounds,width=0.4)
#    if thishem == 'North':
    barlist2 = ax.bar(years+0.2,nums_circs,width=0.4)
    
#    cmap = matplotlib.cm.get_cmap('viridis')
    cmap = cmap_UV_short
#    cmap = cmap_SPEC_short
    clist = [cmap(iii/(len(barlist)-1)) for iii in range(len(barlist))]
    for iii in range(len(barlist)):
        barlist[iii].set_color(clist[iii])
        barlist2[iii].set_color(clist[iii])
#    if thishem == 'North':
    ax.bar(years+0.2,nums_circs,width=0.4, color='None', edgecolor='k', hatch='//')
#    ax.bar(years-0.2,nums_bounds,width=0.4, color='None', edgecolor='k')
    ax.set_xlabel('Year')
    ax.set_ylabel('Number of UVIS images')
    ax.set_xticks(years)
    ax.set_xticklabels(years,rotation=45)
    ax.set_yscale('log')
    ax.set_title('%sern hemisphere'
                 % (thishem))
    ax.set_ylim([5,1000])
    ax.text(0.03,0.92,subf[hemctr],transform=ax.transAxes,
            fontsize=18,fontweight='bold')
plt.savefig('%s/z_num_years.png' % (mainsavepath),
            bbox_inches='tight', dpi=500)
plt.savefig('%s/z_num_years.pdf' % (mainsavepath),
            bbox_inches='tight', dpi=500)
plt.show()
plt.close()




#==========================
# plot angle histograms
fig = plt.figure()
fig.set_size_inches(15,12)
ax1 = fig.add_subplot(221)
ax2 = fig.add_subplot(222, sharey=ax1)
ax3 = fig.add_subplot(223)
ax4 = fig.add_subplot(224, sharey=ax3)
axes = [ax1,ax2,ax3,ax4]
subf = ['(a)','(b)','(c)','(d)']
binsize = 30 #deg
refsys = ['PPO_PHASE_N', 'PPO_PHASE_S']
refsys_math = [r'\Phi_{N}', r'\Phi_{S}']
for hemctr in range(len(hems)):
    thishem = hems[hemctr]   
    for refctr in range(len(refsys)):
        thisref = refsys[refctr]
        ax = axes[2*hemctr+refctr]
        
        angbins = np.linspace(0,2*np.pi,num=int(360/binsize)+1,endpoint=True)
        angcent = angbins[:-1]+np.diff(angbins)/2
        years = np.arange(2007,2018)
        
        nums_bounds = np.zeros((len(angbins)-1, len(years)))
        nums_circs = np.zeros((len(angbins)-1, len(years)))
        
        for aaa in range(len(angbins)-1):
            for yyy in range(len(years)):
                nums_bounds[aaa,yyy] = len(imglist[
                                (imglist['YEAR'] == years[yyy]) &
                                (imglist['HEMISPHERE'] == thishem) &
                                (imglist['BOUNDARY_VALID']) &
                                (imglist['SUBINT_outside_coalescence']) &
                                (imglist[thisref] >= angbins[aaa]) &
                                (imglist[thisref] <= angbins[aaa+1])
                                ])
                nums_circs[aaa,yyy] = len(imglist[
                                (imglist['YEAR'] == years[yyy]) &
                                (imglist['HEMISPHERE'] == thishem) &
                                (imglist['CV_CIRC_VALID']) &
                                (imglist['SUBINT_outside_coalescence']) &
                                (imglist[thisref] >= angbins[aaa]) &
                                (imglist[thisref] <= angbins[aaa+1])
                                ])
        
        cumul = np.zeros_like(angcent)
        for yyy in range(len(years)):
            ax.bar(angcent-np.radians(binsize/5.5), nums_bounds[:,yyy],
                   width=np.radians(binsize/2.75), bottom=cumul,
                   color=clist[yyy])
            cumul += nums_bounds[:,yyy]
            
#        if thishem == 'North':
        cumul = np.zeros(np.shape(angcent))
        for yyy in range(len(years)):
            ax.bar(angcent+np.radians(binsize/5.5), nums_circs[:,yyy],
                   width=np.radians(binsize/2.75), bottom=cumul,
                   color=clist[yyy], edgecolor='k',
                   hatch='//')
            cumul += nums_circs[:,yyy]
        ax.set_xlabel('${}$ (deg)'.format(refsys_math[refctr]))
        ax.set_ylabel('Number of UVIS images')
        ax.set_xticks([0,np.pi/2,np.pi,3*np.pi/2,2*np.pi])
        ax.set_xticklabels([0,90,180,270,360])
        ax.set_xlim([0,2*np.pi])
        ax.set_title('%sern hemisphere'
                     % (thishem))
        ax.text(0.03,0.92,subf[2*hemctr+refctr],transform=ax.transAxes,
            fontsize=18,fontweight='bold')
plt.subplots_adjust(hspace=0.3)
plt.savefig('%s/z_num_phases.png' %  (mainsavepath),
            bbox_inches='tight', dpi=500)
plt.savefig('%s/z_num_phases.pdf' %  (mainsavepath),
            bbox_inches='tight', dpi=500)
plt.show()
plt.close()



