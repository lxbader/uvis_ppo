import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath
uvispath = pr.uvispath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cubehelix
from datetime import datetime
import get_image
import matplotlib
import matplotlib.colors as mcolors
import matplotlib.backends.backend_pdf
import matplotlib.gridspec as gridspec
import matplotlib.patches as ptch
import matplotlib.pyplot as plt
from matplotlib.ticker import NullFormatter
from mpl_toolkits.axes_grid1 import make_axes_locatable
import numpy as np
import os
import scipy.optimize as sopt
import time
import time_conversions as tc
import uvisdb

myc1='royalblue'
myc2='crimson'

def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=100):
    new_cmap = mcolors.LinearSegmentedColormap.from_list(
        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),
        cmap(np.linspace(minval, maxval, n)))
    return new_cmap

binsize = 20

cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)
cmap_UV_short = truncate_colormap(cmap_UV, 0.3, 0.9, 100)
cmap_SPEC = cubehelix.cmap(reverse=False, start=1.8, rot=1, gamma=1.5)

mainsavepath = '%s/Plots/PPO/%s' % (boxpath,time.strftime('%Y%m%d%H%M%S'))
rotimgpath = '%s/rot_img_ppo_beat.npz' % uvispath
ppointpath = '%s/imglists/ppo_int.npz' % boxpath

savepath = '%s/%s' % (mainsavepath, binsize)
if not os.path.exists(savepath):
    os.makedirs(savepath)
    
hems = ['North', 'South']
refsys = ['PPO_N', 'PPO_S']
refsys_math = ['\Phi_\mathrm{N}','\Phi_\mathrm{S}']
beatphase = ['00all','01inphase','02SaheadN','03antiphase','04SbehindN']
datefmt = '%Y-%jT%H-%M-%S'

def Gaussian(x,a,b,c,d):
    inner = -(x-b)**2/2/c**2
    return a*np.exp(inner)+d

def sinusoidDEG(x,ampl,phase,offset):
    return ampl*np.sin((x+phase)/180*np.pi)+offset


#===================================
# get UVIS image database and munge
    
udb = uvisdb.UVISDB()
imglist = udb.currentDF.copy()
# number of observations
NOBS = imglist.shape[0]
# center time
imglist['IMGTIME'] = imglist[['ET_START','ET_STOP']].mean(axis=1)
# mark images as used
imglist['IMGUSED'] = 0
        
# which images correspond to which subinterval
subintervnames = ['SUBINT_all', 'SUBINT_outside_coalescence', 'SUBINT_during_coalescence']
for iii in range(len(subintervnames)):
    current = subintervnames[iii]
    imglist[current] = 0
    if current == 'SUBINT_all':
        imglist[current] = 1
    elif current == 'SUBINT_outside_coalescence':
        imglist[current][(imglist['IMGTIME'] <= tc.str2et('2013-181T00-00-00', datefmt)) | 
                         (imglist['IMGTIME'] >= tc.str2et('2014-181T00-00-00', datefmt))] = 1
    elif current == 'SUBINT_during_coalescence':
        imglist[current][(imglist['IMGTIME'] > tc.str2et('2013-181T00-00-00', datefmt)) & 
                         (imglist['IMGTIME'] < tc.str2et('2014-181T00-00-00', datefmt))] = 1
    
# intensity limits
# [subinterv,hem,min/max]
silim = np.zeros((len(subintervnames),2,2))
silim[:,0,0] = [5,5,5]
silim[:,1,0] = [2,2,5]
silim[:,0,1] = [30,30,20]
silim[:,1,1] = [25,25,20]
    
# get some image stats
#imgpaths = imagelist['FILEPATH']
#imgtimes = np.mean(imagelist['ET_START_STOP'], axis=1)
#imghems = imagelist['HEMISPHERE']
#imgdist = imagelist['POS_KRTP'][:,0]
#        subintervind[np.where(imgdist>55)[0],:] = 0
                                
# Create histograms of LT-PPO occurrences and intensities
histlonbins = np.linspace(0,360,num=int(360/binsize+1))
histLTbins = np.linspace(0,24,num=int(360/binsize+1))

histshape = (len(subintervnames),len(hems),len(beatphase),len(refsys),2,
             len(histlonbins)-1,len(histlonbins)-1)
abshist = np.zeros(histshape)
abslist = np.full(histshape+(10,), np.nan)
relhist = np.zeros(histshape)
numhist = np.zeros(histshape)

# Note down number of used images
numshape = (NOBS, len(subintervnames), len(hems), len(beatphase), len(refsys))
num_of_images = np.zeros(numshape)
        
# open first file to get an idea of the sizing
image, _, _ = get_image.getRedUVIS('{}/{}'.format(uvispath,
                                imglist['FILEPATH'].iloc[0]))

# image lat and lon bins / limits        
lonbins = np.linspace(0,360, num=int(np.shape(image)[0]+1))
latbins = np.linspace(7,23,num=int(np.shape(image)[1]*16/30+1))
# image lon centers
loncenters = lonbins[:-1]+np.mean(np.diff(lonbins))/2
latcenters = latbins[:-1]+np.mean(np.diff(latbins))/2
# ... indices, sorted according to histogram bins
# after rotating an image according to frame X,
# we can just take all histind==1 to fill the first histogram bin
histind = np.digitize(loncenters, histlonbins)-1

# LT, PPO N, PPO S indices for one non-rotated image
allind = np.zeros((2,2,len(histind)))

# determine beat phase for all images        
# calculate PPO beat phase
imglist['PPO_SminN'] = (imglist['PPO_PHASE_S'] - imglist['PPO_PHASE_N']) % (2*np.pi)
imglist['PPO_BEAT_PHASE'] = 0
# in phase
imglist['PPO_BEAT_PHASE'][(imglist['PPO_SminN']<=1/4*np.pi) |
                            (imglist['PPO_SminN']>7/4*np.pi)] = 1
# S leading N
imglist['PPO_BEAT_PHASE'][(imglist['PPO_SminN']>1/4*np.pi) &
                            (imglist['PPO_SminN']<=3/4*np.pi)] = 2
# antiphase
imglist['PPO_BEAT_PHASE'][(imglist['PPO_SminN']>3/4*np.pi) &
                            (imglist['PPO_SminN']<=5/4*np.pi)] = 3
# S lagging N
imglist['PPO_BEAT_PHASE'][(imglist['PPO_SminN']>5/4*np.pi) &
                            (imglist['PPO_SminN']<=7/4*np.pi)] = 4
       



try:
#    raise Exception
    tmp = np.load(ppointpath)
    abshist = tmp['abshist'][()]
    abslist = tmp['abslist'][()]
    relhist = tmp['relhist'][()]
    numhist = tmp['numhist'][()]
    num_of_images = tmp['num_of_images'][()]
except:
    usedimg = np.zeros((NOBS))         
    for imgctr in range(0,NOBS):
        if not imgctr % 10:
            print(imgctr)
            
        # skip images where only one phase value is available
        if imglist.loc[imgctr,'PPO_BEAT_PHASE'] == 0:
            continue
        
        # get image
        red_image, _ = get_image.getRedUVIS('{}/{}'.format(uvispath,
                                                  imglist['FILEPATH'].iloc[imgctr]), minangle=15)
        if not np.all(np.shape(red_image)==((len(lonbins)-1,(len(latbins)-1)*30/16))):
            continue
        # cut out interesting part with 7deg<colat<23deg
        if imglist['HEMISPHERE'].iloc[imgctr] == 'North':
            validhems = [0]
        else:
            validhems = [1]
        minndx = int(np.shape(image)[1]/30*latbins[0])
        maxndx = int(np.shape(image)[1]/30*latbins[-1])
        cut = red_image[:,minndx:maxndx]
        
        if not np.any(np.isfinite(cut)):
            continue

        # check in which time intervals the image falls
        validsubinterv = np.where(imglist[subintervnames].iloc[imgctr,:].tolist())[0]
        
        # calculate PPO phases and roll images/indices accordingly
        ppo_N = imglist['PPO_PHASE_N'].iloc[imgctr]
        ppo_S = imglist['PPO_PHASE_S'].iloc[imgctr]
        
        refphases = [ppo_N,ppo_S]
        refphases_act = [ppo_N+np.pi, ppo_S+np.pi]
        refphases_act = np.mod(refphases_act, 2*np.pi)
        for iii in range(len(refphases)):
            if np.isnan(refphases[iii]):
                allind[iii,:,:] = np.nan
                continue
            shift = -int(np.floor((len(lonbins)-1)*refphases[iii]/(2*np.pi)))                
            allind[iii,0,:] = np.roll(histind, shift)[::-1]
            allind[iii,1,:] = np.digitize(refphases_act[iii]*180/np.pi,histlonbins)-1

        for refctr in range(len(refsys)):                
            for LTlonctr in range(len(histlonbins)-1):
                for PPOlonctr in range(len(histlonbins)-1):
                    for typectr in range(2):
                        tmp = np.where((histind==LTlonctr)
                                            *(allind[refctr,typectr,:]==PPOlonctr))[0]
                        # if tmp smaller than *number of bins per 5 deg lon*
                        # aka if section covers less than 5 deg lon
                        if np.size(tmp)<(len(lonbins)-1)/72:
                            continue
                        thissection = np.take(cut, tmp, axis=0)
                        # if more than one lon bin, take mean across lon axis
                        if thissection.ndim > 1:
                            thissection = np.nanmean(thissection, axis = 0)
                        # no data in section
                        if not np.any(thissection>0):
                            continue
                        # less than half the section has valid data
                        if np.sum(thissection>0) < len(thissection)/2:
                            continue
                        # take valid parts of the section
                        tmp = thissection[np.where(thissection>0)]
                        if np.nanmax(tmp>100):
                            continue
                        # mark image as used
                        # DO NOT PUT DIRECTLY IN DATAFRAME, LOOPS CAN KILL
#                        imglist['IMGUSED'].iloc[imgctr] = 1
                        usedimg[imgctr] = 1
                        # add maximum or whatever to histogram, increase counter
                        for bbb in [0,imglist['PPO_BEAT_PHASE'].iloc[imgctr]]:
                            relhist[validsubinterv,
                                    validhems,
                                    bbb,
                                    refctr,
                                    typectr,
                                    LTlonctr,
                                    PPOlonctr] += np.nanmax(tmp)/np.nanmax(cut)
                            abshist[validsubinterv,
                                    validhems,
                                    bbb,
                                    refctr,
                                    typectr,
                                    LTlonctr,
                                    PPOlonctr] += np.nanmax(tmp)
                            numhist[validsubinterv,
                                    validhems,
                                    bbb,
                                    refctr,
                                    typectr,
                                    LTlonctr,
                                    PPOlonctr] += 1
                            num_of_images[imgctr,validsubinterv,validhems,bbb,refctr] = 1
                            
                            # median stuff
                            for ijk in validsubinterv:
                                currentctr = int(numhist[ijk,
                                        validhems,
                                        bbb,
                                        refctr,
                                        typectr,
                                        LTlonctr,
                                        PPOlonctr])
                                abslist[ijk,
                                        validhems,
                                        bbb,
                                        refctr,
                                        typectr,
                                        LTlonctr,
                                        PPOlonctr,currentctr] = np.nanmax(tmp)
                                if currentctr == np.shape(abslist)[-1]-1:
                                    abslist = np.concatenate([abslist, np.full(np.append(np.shape(numhist),10), np.nan)], axis=-1)
    imglist['IMGUSED'] = usedimg
    np.savez(ppointpath,
             abshist=abshist,
             abslist=abslist,
             relhist=relhist,
             numhist=numhist,
             num_of_images=num_of_images)
    
#%%

#sys.exit()       
def log_10_product(x, pos):
    # make labels in log plots disappear so I can put my own
    return ''
formatter = plt.FuncFormatter(log_10_product)

for tag in ['ABS']:
    if tag == 'REL':
        inthistmean = relhist/numhist*100
    elif tag == 'ABS':
        inthistmean = abshist/numhist
#        inthistmean = np.nanmedian(abslist, axis=-1)
        # get mean from list of all values after excluding min/max xx%
        cleanabshist = np.full_like(abshist, np.nan)♣
        mins = np.nanpercentile(abslist, 1, axis=-1)
        maxs = np.nanpercentile(abslist, 99, axis=-1)                
        sh = np.shape(abslist)
        for aaa in range(sh[0]):
            for bbb in range(sh[1]):
                for ccc in range(sh[2]):
                    for ddd in range(sh[3]):
                        for eee in range(sh[4]):
                            for fff in range(sh[5]):
                                for ggg in range(sh[6]):
                                    tmpmin = mins[aaa,bbb,ccc,ddd,eee,fff,ggg]
                                    tmpmax = maxs[aaa,bbb,ccc,ddd,eee,fff,ggg]
                                    tmpval = abslist[aaa,bbb,ccc,ddd,eee,fff,ggg,:]
                                    argtmp = np.where((tmpval<tmpmax)*(tmpval>tmpmin))[0]
                                    cleanabshist[aaa,bbb,ccc,ddd,eee,fff,ggg] = np.mean(tmpval[argtmp])
        inthistmean = cleanabshist                       
    plot_LT = histLTbins
    plot_ref = np.concatenate([histlonbins, histlonbins[1:]+histlonbins[-1]])
    plot_hist = np.concatenate([inthistmean, inthistmean], axis=-1)
    plot_num = np.concatenate([numhist, numhist], axis=-1)
              
    # for all subintervals
    for sictr in range(0,len(subintervnames)):
#    for sictr in range(1,2):
        thissic = subintervnames[sictr]
        
        fullsavepath = '{}/{}'.format(savepath,thissic)
        if not os.path.exists(fullsavepath):
            os.makedirs(fullsavepath)
    
        # for both hemispheres
        for hemctr in range(len(hems)):
            thishem = hems[hemctr]
            
            pdf = matplotlib.backends.backend_pdf.PdfPages('%s/LON_%s_%s.pdf' %
                                                (fullsavepath,tag,thishem))
                        
            # for both PPO reference frames
            for refctr in range(len(refsys)):
                thisref = refsys[refctr]
                
                # make an array to store the psi-intensity histograms in
                psi_int_hists = np.full((len(beatphase),len(plot_ref)),np.nan)
                psi_int_nums = np.zeros((len(beatphase)))
                # maxloc, maxloc_std, pkpk, pkpk_std
                psi_int_params = np.zeros((len(beatphase),4))
                
                # for all beat periods
                for beatctr in range(len(beatphase)):
                    thisbeat = beatphase[beatctr]
                    
                    # for both LT-Phi and LT-Psi histograms
                    for typectr in range(1):
                        
#                        # Get image times and phase angles
#                        if thisref=='PPO_N':
#                            angles = imagelist['PPO_N_S'][np.where(imagelist['HEMISPHERE']==thishem)[0],0]
#                        elif thisref=='PPO_S':
#                            angles = imagelist['PPO_N_S'][np.where(imagelist['HEMISPHERE']==thishem)[0],1]
#                            
#                        times = np.mean(imagelist['ET_START_STOP'][np.where(imagelist['HEMISPHERE']==thishem)[0]], axis=1)
#                        valid = np.where(np.isfinite(angles))[0]
#                        times = times[valid]
#                        angles = angles[valid]
                        
                        # Plot 2D intensity statistics
                        for scale in ['lin','log']:
                            if tag=='REL' and scale=='log':
                                continue
                            if tag=='ABS' and scale=='lin':
                                continue
                            if not np.any(np.isfinite(inthistmean[sictr,hemctr,beatctr,refctr,typectr,:,:])):
                                continue
                            
                            data = plot_hist[sictr,hemctr,beatctr,refctr,typectr,:,:]
                            data = np.ma.masked_invalid(data)
                            if not np.ma.count(data):
                                continue
                                                                     
                            # set up figure
                            fig = plt.figure()
                            fig.set_size_inches(8,8)
                            ax = fig.add_subplot(111)
                            divider = make_axes_locatable(ax)
                            axHistx = divider.append_axes("top", 1.2, pad=0.05, sharex=ax)
                            axHisty = divider.append_axes("right", 1.2, pad=0.05, sharey=ax)
                            axcbar = divider.append_axes("right", size="5%", pad=0.2)
                            # add colormap histogram
                            formatter = NullFormatter()
                            ticks = np.array([2,5,10,15,20,25,30,35,40])
                            ticks = ticks[np.where((ticks>=silim[sictr,hemctr,0])*(ticks<=silim[sictr,hemctr,1]))]
                            if scale=='log':
                                quad = ax.pcolormesh(plot_LT,plot_ref,np.log10(data.T),
                                                     vmin=np.log10(silim[sictr,hemctr,0]),
                                                     vmax=np.log10(silim[sictr,hemctr,1]),
                                                     cmap=cmap_UV)
                                cbar = plt.colorbar(quad, cax=axcbar, format=NullFormatter())
                                logticks = np.concatenate([np.arange(1,10), np.arange(10,40,5)])
                                logticks = logticks[np.where((logticks>=silim[sictr,hemctr,0])*(logticks<=silim[sictr,hemctr,1]))]
                                logticklabels = np.array(['{}'.format(iii) if iii in ticks else '' for iii in logticks])
                                cbar.set_ticks(np.log10(logticks))
                                cbar.set_ticklabels(logticklabels)
                            else:
                                quad = ax.pcolormesh(plot_LT,plot_ref,data.T,
                                                     cmap=cmap_UV)
                                cbar = plt.colorbar(quad, cax=axcbar)
#                                cbar.set_ticks(ticks)
#                                cbar.set_ticklabels(['{}'.format(iii) for iii in ticks])
#                            cbar.ax.minorticks_off()
#                            cbar.ax.majorticks_off()
#                            cbar.ax.xaxis.set_major_formatter(NullFormatter())
#                            cbar.ax.xaxis.set_minor_formatter(NullFormatter())
#                            for label in cbar.ax.xaxis.get_minorticklabels():
#                                label.set_visible(False)
#                            cbar.ax.xaxis.set_ticks([], minor=True)
#                            cbar.set_ticks(ticks)
#                            cbar.set_ticklabels(['{}'.format(iii) for iii in ticks])
#                            cbar.update_ticks()
                            
                            # label some stuff
                            if tag=='REL':
                                cbar.set_label('Avg intensity maximum (%)',rotation=270,labelpad=15)
                            else:
                                cbar.set_label('Mean intensity maximum (kR)',rotation=270,labelpad=15)
                            ax.set_xlabel('LT (hours)')
                            if typectr==0:
                                ax.set_ylabel(r'$\Psi_\mathrm{%s} = %s-\varphi$ (deg)' % (thisref[-1],refsys_math[refctr]))
                                if 'PPO' in thisref:
                                    for iii in range(3):
                                        if thishem=='North':
                                            ax.plot([0,24],np.array([90,90])+360*iii, 'w--')
                                        else:
                                            ax.plot([0,24],np.array([-90,-90])+360*iii, 'w--')
                            else:
                                ax.set_ylabel('${}$ (deg)'.format(refsys_math[refctr]))
                                if 'PPO' in thisref:
                                    for iii in range(3):
                                        if thishem=='North':
                                            ax.plot([0,24],np.array([-90,270])+360*iii, 'w--')
                                        else:
                                            ax.plot([0,24],np.array([-270,90])+360*iii, 'w--')
                                
                            plottable_num = int(np.nansum(num_of_images[:,sictr,hemctr,beatctr,refctr]))
                        
                            # top histogram
                            LTmid = plot_LT+np.mean(np.diff(plot_LT))/2
                            LTmid = np.append([0], LTmid)
                            LTavg = np.nanmean(plot_hist[sictr,hemctr,beatctr,refctr,typectr,:,:], axis=-1)
                            LTavg = np.append(LTavg, [LTavg[0]])
                            LTavg = np.append([LTavg[0]], LTavg)
                            LTerr = np.nanstd(plot_hist[sictr,hemctr,beatctr,refctr,typectr,:,:], axis=-1)
                            LTerr = np.append(LTerr, [LTerr[0]])   
                            LTerr = np.append([LTerr[0]], LTerr)
                            axHistx.step(LTmid,LTavg,where='mid', linewidth=1.5,
                                         label='{} ({})'.format(thissic, plottable_num),
                                         color='k')
#                                    axHistx.errorbar(LTmid, LTavg,yerr=LTerr, fmt=None, linewidth=1, color='k', capsize=1, capthick=1)
                            timelims = np.zeros((3,2))
                            timelims[0,:] = [0,24]
                            timelims[1,:] = [5,12]
                            timelims[2,:] = [17,24]
                            cc_lt = ['k',myc1,myc2]
                            
                            # side histogram
                            cent_LT = plot_LT[:-1]+np.diff(plot_LT)/2
                            for iii in range(np.shape(timelims)[0]):
                                argvalid = np.where((cent_LT>timelims[iii,0])*(cent_LT<timelims[iii,1]))[0]
                                label = '%.1f-%.1f LT' % (plot_LT[argvalid[0]], plot_LT[argvalid[-1]+1])
                                REFlow = plot_ref
                                REFavg = np.nanmean(plot_hist[sictr,hemctr,beatctr,refctr,typectr,argvalid,:], axis=-2)
                                REFavg = np.append(REFavg, [REFavg[0]])
                                REFerr = np.nanstd(plot_hist[sictr,hemctr,beatctr,refctr,typectr,argvalid,:], axis=-2)
                                REFerr = np.append(REFerr, [REFerr[0]])
                                axHisty.step(REFavg,REFlow,where='pre', linewidth=1.5,
                                             label=label, color=cc_lt[iii])
                                if iii:
                                    rect = ptch.Rectangle((plot_LT[argvalid[0]]+0.1,4),
                                                          plot_LT[argvalid[-1]+1]-plot_LT[argvalid[0]]-0.2,
                                                          713,
                                                          linewidth=2,linestyle='-',
                                                          edgecolor='w',facecolor='none',
                                                          alpha=0.5, zorder=5)
                                    ax.add_patch(rect)
                                    rect = ptch.Rectangle((plot_LT[argvalid[0]]+0.1,4),
                                                          plot_LT[argvalid[-1]+1]-plot_LT[argvalid[0]]-0.2,
                                                          713,
                                                          linewidth=2,linestyle='--',
                                                          edgecolor=cc_lt[iii],facecolor='none', zorder=8)
                                    ax.add_patch(rect)

                                # save this histogram
                                if typectr==0 and iii==0:
                                    psi_int_hists[beatctr,:] = REFavg
                                    psi_int_nums[beatctr] = plottable_num
                                    
                                    # fit sinusoid and  plot
                                    xdata = plot_ref[:-1]+np.diff(plot_ref)/2
                                    ydata = REFavg[:-1]
                                    opt, cov = sopt.curve_fit(sinusoidDEG, xdata, ydata)
                                    # plot fit
                                    xs = np.linspace(0,720,num=100,endpoint=True)
                                    ys = sinusoidDEG(xs,opt[0],opt[1],opt[2])
                                    axHisty.plot(ys,xs,c='0.4',linewidth=1.5)
                                    # plot hoizontal lines indicating the peaks
                                    peakloc = (90-opt[1])%360
                                    if opt[0] < 0:
                                        peakloc = (peakloc+180)%360
                                    for loc in [peakloc,peakloc+360]:
                                        axHisty.axhline(loc, c='0.4', ls='-.', lw=1.5)
                                    # remember fit parameters
                                    pkpk = 2*np.abs(opt[0])
                                    pkpk_std = np.sqrt(np.diag(cov))[0]*2
                                    maxloc = np.copy(peakloc)
                                    maxloc_std = np.sqrt(np.diag(cov))[1]
                                    
                                    psi_int_params[beatctr,:] = [maxloc,maxloc_std,pkpk,pkpk_std]
                        
                        
#                                axHisty.errorbar(REFavg,REFlow,xerr=REFerr, fmt=None, linewidth=1, color='k', capsize=1, capthick=1)
                            
                            # finish labeling and stuff
                            if tag=='REL':
                                axHistx.set_ylabel('Mean intensity\n maximum (%)')
                                axHisty.set_xlabel('Mean intensity\n maximum (%)')
                            else:
                                axHistx.set_ylabel('Mean intensity\n maximum (kR)')
                                axHisty.set_xlabel('Mean intensity\n maximum (kR)')
#                                if tag == 'REL':
#                                    axHistx.set_title('Average relative max intensity, {}, {}, {}'.format(
#                                            thishem,thisppo.replace('_',' '), re.sub(r'\d*', '', thisbeat)))
#                                elif tag == 'ABS':
#                                    axHistx.set_title('Average absolute max intensity, {}, {}, {}'.format(
#                                            thishem,thisppo.replace('_',' '), re.sub(r'\d*', '', thisbeat)))
                            axHistx.tick_params(axis='x', direction='in')
                            plt.setp(axHistx.get_xticklabels(), visible=False)
                            axHisty.tick_params(axis='y', direction='in')
                            plt.setp(axHisty.get_yticklabels(), visible=False)
                            ax.set_xlim([0,24])
                            ax.set_xticks([0,6,12,18,24])
                            ax.set_ylim([0,720])
                            ax.set_yticks([0,90,180,270,360,450,540,630,720])
                            thissubf = '(a)' if thisref=='PPO_N' else '(b)'
                            axHistx.text(0.02,0.78,thissubf, transform=axHistx.transAxes,fontsize=18,fontweight='bold')
                            axHistx.text(1.05,0.6,'Number of\nimages: {}'.format(plottable_num),
                                         transform=axHistx.transAxes,fontsize=11)
                            axHistx.text(1.05,0.1,
                                         r'pk-pk: $%.1f\pm%.1f$ kR' % (pkpk, np.round(pkpk_std, decimals=1))
                                         +'\n'+r'$\Psi_\mathrm{%s,max}: %d\pm%d$ deg' % (thishem[0], int(maxloc), int(maxloc_std)),
                                         transform=axHistx.transAxes,fontsize=11, color='0.3')
#                                    axHistx.text(1.05,0.2,
#                                                 r'$\Psi_\mathrm{%s,max}: %d$ deg' % (thishem[0], int(maxloc)),
#                                                 transform=axHistx.transAxes,fontsize=11)
                            
                            axHistx.grid()
                            axHisty.grid()
                            axHisty.set_xscale('log')
                            if thishem=='North':
                                axHistx.set_ylim([5,25])
                                axHistx.set_yticks([10,15,20,25])
                                axHisty.set_xlim([6,30])
                                axHisty.set_xticks([10,20,30])
                                axHisty.set_xticklabels(['10','20','30'])
                                axHisty.get_xaxis().set_minor_formatter(formatter)
                            else:
                                axHistx.set_ylim([4,15])
                                axHistx.set_yticks([5,10,15])
                                axHisty.set_xlim([3,20])
                                axHisty.set_xticks([5,10,20])
                                axHisty.set_xticklabels(['5','10','20'])
                                axHisty.get_xaxis().set_minor_formatter(formatter)
#                                    if thissic == 'during coalescence':
#                                        axHistx.set_ylim([0,15])
#                                        axHistx.set_yticks([0,5,10,15])
#                                        axHisty.set_xlim([0,15])
#                                        axHisty.set_xticks([0,5,10,15])
                            
                            if beatctr == 0:
                                if typectr==0:
                                    plt.savefig('%s/LON_%s_%s_%s_%s_%s.png' %
                                                (fullsavepath,tag,thishem,thisref,thisbeat,scale),
                                                bbox_inches='tight', dpi=500)
                                    plt.savefig('%s/LON_%s_%s_%s_%s_%s.pdf' %
                                                (fullsavepath,tag,thishem,thisref,thisbeat,scale),
                                                bbox_inches='tight', dpi=500)
                                    pdf.savefig(fig)
                                else:
                                    plt.savefig('%s/PHASE_%s_%s_%s_%s_%s.png' %
                                                (fullsavepath,tag,thishem,thisref,thisbeat,scale),
                                                bbox_inches='tight', dpi=500)
                                    plt.savefig('%s/PHASE_%s_%s_%s_%s_%s.pdf' %
                                                (fullsavepath,tag,thishem,thisref,thisbeat,scale),
                                                bbox_inches='tight', dpi=500)
                            plt.show()
                            plt.close()
                # line histogram comparison of beat periods
                if thishem=='North' and thisref=='PPO_N':
                    save_nums = np.copy(psi_int_nums)
                fig, [ax1,ax2] = plt.subplots(nrows=2, sharex=True, sharey=True)
                fig.set_size_inches(10,6)
                gind = [0,1,3,0,2,4]
                axes = [ax1,ax1,ax1,ax2,ax2,ax2]
                names = ['all beat periods','in phase','in antiphase',
                         'all beat periods','S leading N','S lagging N']
                cs = ['k',myc1,myc2,'k',myc1,myc2]
                for iii in range(len(gind)):
                    if not np.any(np.isfinite(psi_int_hists[gind[iii],:])):
                        continue
                    # plot data
                    axes[iii].step(plot_ref, psi_int_hists[gind[iii],:], where='post',
                             color=cs[iii], linewidth=1.3,
                             label='{} ({})'.format(names[iii], int(psi_int_nums[gind[iii]])))
                    # fit sinusoid
                    xdata = plot_ref[:-1]+np.diff(plot_ref)/2
                    ydata = psi_int_hists[gind[iii],:-1]
                    opt, cov = sopt.curve_fit(sinusoidDEG, xdata, ydata)
                    # plot fit
                    xs = np.linspace(0,720,num=100,endpoint=True)
                    ys = sinusoidDEG(xs,opt[0],opt[1],opt[2])
                    axes[iii].plot(xs,ys,c=cs[iii],linewidth=2.5)
                    # plot vertical lines indicating the peaks
                    peakloc = (90-opt[1])%360
                    if opt[0] < 0:
                        peakloc = (peakloc+180)%360
                    for loc in [peakloc,peakloc+360]:
                        axes[iii].axvline(loc, c=cs[iii], ls='-', lw=2)
                    if iii in [1,2,4,5]:
                        pp = psi_int_params[gind[iii],:]
                        axes[iii].text(0.01, 0.06 if (iii-1)%3 else 0.21,
                                r'pk-pk: $%.1f\pm%.1f$, $\Psi_\mathrm{N,max}$: $%d\pm%d$ deg' % (pp[2],pp[3],int(pp[0]),int(pp[1])),
                                color=cs[iii],
                                transform=axes[iii].transAxes,fontsize=10, bbox={'facecolor':'0.7', 'pad':3, 'alpha':0.95})
                        
                # settings
                axes = [ax1,ax2]
                for ax in axes:
                    ax.set_xlim([0,720])
                    ax.legend(loc=1)
                    ax.grid()
                    ax.set_ylabel('Mean intensity maximum (kR)')
                    exp_angle = 90 if thishem=='North' else 270
                    ax.axvline(exp_angle, 0, 1, color='0.3', linestyle='--')
                    ax.axvline(exp_angle+360, 0, 1, color='0.3', linestyle='--')
                ax1.text(0.02,0.90,'(a)',transform=ax1.transAxes,
                         fontsize=18,fontweight='bold')
                ax2.text(0.02,0.90,'(b)',transform=ax2.transAxes,
                         fontsize=18,fontweight='bold')
                ax2.set_xticks([0,90,180,270,360,450,540,630,720])
                ax2.set_xlabel(r'$\Psi_\mathrm{%s} = %s-\varphi$ (deg)' % (thisref[-1], refsys_math[refctr]))
                ax1.set_title('{}ern hemisphere'.format(thishem))
                plt.subplots_adjust(hspace=0.05)
                plt.savefig('%s/BEAT_%s_%s.png' %
                                            (fullsavepath,thishem,thisref),
                                            bbox_inches='tight', dpi=500)
                plt.savefig('%s/BEAT_%s_%s.pdf' %
                                            (fullsavepath,thishem,thisref),
                                            bbox_inches='tight', dpi=500)
                plt.show()
                plt.close()
                
            pdf.close()                                                                    
        # plot year histograms
        fig = plt.figure()
        fig.set_size_inches(15,5)
        ax1 = fig.add_subplot(121)
        ax2 = fig.add_subplot(122, sharey=ax1)
        axes = [ax1,ax2]
        subf = ['(a)','(b)']
        
        for hemctr in range(len(hems)):
            thishem = hems[hemctr]   
            ax = axes[hemctr]
            # Get image times
            valid = np.where(np.sum(num_of_images[:,sictr,hemctr,0,:], axis=-1))[0]
            times = imglist['IMGTIME'].iloc[valid]
            # year histograms
            years = np.arange(2007,2019)
            dtbins = np.array([datetime(iii,1,1) for iii in years])
            etbins = tc.datetime2et(dtbins)
            etcenters = etbins[:-1]+np.diff(etbins)/2
            hist, trash = np.histogram(times,etbins)
            barlist = ax.bar(etcenters,hist,width=np.mean(np.diff(etbins))*0.8)
            cmap = matplotlib.cm.get_cmap('viridis')
            cmap = cmap_UV_short
            clist = [cmap(iii/(len(barlist)-1)) for iii in range(len(barlist))]
            for iii in range(len(barlist)):
                barlist[iii].set_color(clist[iii])
            ax.set_xlabel('Year')
            ax.set_ylabel('Number of UVIS images')
            ax.set_xticks(etcenters)
            ax.set_xticklabels(years[:-1],rotation=45)
            ax.set_yscale('log')
            ax.set_title('%sern hemisphere'
                         % (thishem))
            ax.set_ylim([10,1000])
            ax.text(0.03,0.92,subf[hemctr],transform=ax.transAxes,
                    fontsize=18,fontweight='bold')
        plt.savefig('%s/z_num_years.png' % (fullsavepath),
                    bbox_inches='tight', dpi=500)
        plt.savefig('%s/z_num_years.pdf' % (fullsavepath),
                    bbox_inches='tight', dpi=500)
        plt.show()
        plt.close()
        
        # plot angle histograms
        fig = plt.figure()
        fig.set_size_inches(15,12)
        ax1 = fig.add_subplot(221)
        ax2 = fig.add_subplot(222, sharey=ax1)
        ax3 = fig.add_subplot(223)
        ax4 = fig.add_subplot(224, sharey=ax3)
        axes = [ax1,ax2,ax3,ax4]
        subf = ['(a)','(b)','(c)','(d)']
        
        for hemctr in range(len(hems)):
            thishem = hems[hemctr]   
            for refctr in range(len(refsys)):
                thisref = refsys[refctr]
                ax = axes[2*hemctr+refctr]
                
                valid = np.where(num_of_images[:,sictr,hemctr,0,refctr])[0]
                
                if thisref=='PPO_N':
                    angles = imglist['PPO_PHASE_N']
                elif thisref=='PPO_S':
                    angles = imglist['PPO_PHASE_S']
                    
                times = imglist['IMGTIME'].iloc[valid]
                angles = ((angles[valid]+np.pi) % (2*np.pi))
                                                
                # angle histogram
                angbins = np.linspace(0,2*np.pi,num=int(360/binsize)+1,endpoint=True)
                angcent = angbins[:-1]+np.diff(angbins)/2
                cumul = np.zeros_like(angcent)
                for iii in range(len(etbins)-1):
                    tmp = np.where((times>etbins[iii])*(times<etbins[iii+1]))[0]
                    hist, trash = np.histogram(angles.iloc[tmp],angbins)
                    ax.bar(angcent,hist,width=np.mean(np.diff(angbins))*0.8,bottom=cumul,
                           color=clist[iii])
                    cumul += hist
                ax.set_xlabel('${}$ (deg)'.format(refsys_math[refctr]))
                ax.set_ylabel('Number of UVIS images')
                ax.set_xticks([0,np.pi/2,np.pi,3*np.pi/2,2*np.pi])
                ax.set_xticklabels([0,90,180,270,360])
                ax.set_xlim([0,2*np.pi])
                ax.set_title('%sern hemisphere'
                             % (thishem))
                ax.text(0.03,0.92,subf[2*hemctr+refctr],transform=ax.transAxes,
                    fontsize=18,fontweight='bold')
        plt.subplots_adjust(hspace=0.3)
        plt.savefig('%s/z_num_phases.png' %  (fullsavepath),
                    bbox_inches='tight', dpi=500)
        plt.savefig('%s/z_num_phases.pdf' %  (fullsavepath),
                    bbox_inches='tight', dpi=500)
        plt.show()
        plt.close()
                
        # plot beat phase averages
        if sictr in [1]:
            try:
                raise Exception
                rot_img = np.load(rotimgpath)['rot_img'][()]
            except:                
                rot_img = np.zeros((NOBS,)+np.shape(get_image.getRedUVIS('{}/{}'.format(uvispath,
                                                   imglist['FILEPATH'].iloc[0]))[0]))
                print('Rotating images into PPO frame')
                for imgctr in range(NOBS):                    
                    red_image, _, _ = get_image.getRedUVIS('{}/{}'.format(uvispath,
                                                   imglist['FILEPATH'].iloc[imgctr], minangle=25))

                    # calculate PPO phase and roll image accordingly
                    if imglist['HEMISPHERE'].iloc[imgctr] == 'North':
                        ppo = imglist['PPO_PHASE_N'].iloc[imgctr]
                    else:
                        ppo = imglist['PPO_PHASE_S'].iloc[imgctr]
                    if np.isnan(ppo):
                        continue
                    shift = -int(np.floor((np.shape(red_image)[0])*ppo/(2*np.pi)))
                    rot_img[imgctr,:,:] = np.roll(red_image, shift, axis=0)
                np.savez(rotimgpath,
                    rot_img=rot_img)
            
            beatavgs = np.zeros((4,)+np.shape(rot_img)[1:])
            beatmeds = np.zeros((4,)+np.shape(rot_img)[1:])
            beatnums = np.zeros((4))
            print('Calculating beat period image averages')
            for bctr in range(1,5):
                validind = np.where(imglist['SUBINT_outside_coalescence'] &
                                    (imglist['PPO_BEAT_PHASE']==bctr) & 
                                    (imglist['HEMISPHERE']=='North'))[0]
                beatnums[bctr-1] = len(validind)
                data = rot_img[validind,:,:]
                data_clean = np.copy(data)
                tmpmin = np.nanpercentile(data, 0, axis=0)
                tmpmax = np.nanpercentile(data, 100, axis=0)
                data_clean[np.where(np.logical_or(data<tmpmin, data>tmpmax))] = np.nan
    
                beatavgs[bctr-1,:,:] = np.nanmean(data_clean, axis=0)[::-1,:]
                beatmeds[bctr-1,:,:] = np.nanmedian(data, axis=0)[::-1,:]
                
            def markRegion(ax,minang,maxang):
                ec = '0.9'
                alp = 0.8
                minang = minang*np.pi/180
                maxang = maxang*np.pi/180
                if maxang>minang:
                    ax.fill_between(np.linspace(minang, maxang, num=100),0,30,
                                    facecolor='none', edgecolor=ec, hatch='xx', alpha=alp)
                else:
                    ax.fill_between(np.linspace(minang, 2*np.pi, num=100),0,30,
                                    facecolor='none', edgecolor=ec, hatch='xx', alpha=alp)
                    ax.fill_between(np.linspace(0, maxang, num=100),0,30,
                                    facecolor='none', edgecolor=ec, hatch='xx', alpha=alp)
                
            for ttt in ['avg','med']:
                fig = plt.figure()
                fig.set_size_inches(13,12)
                gs = gridspec.GridSpec(2,3, width_ratios=[6,6,0.5])
                gs.update(hspace=0.2, wspace=0.2)
                ax1 = plt.subplot(gs[0,0], projection='polar')
                ax2 = plt.subplot(gs[0,1], projection='polar')
                ax3 = plt.subplot(gs[1,0], projection='polar')
                ax4 = plt.subplot(gs[1,1], projection='polar')
                cax = plt.subplot(gs[0:,2])
                axes = [ax1,ax3,ax2,ax4]
                subf = ['(a)','(c)','(b)','(d)']
                titles = ['in phase', 'S leading N',
                          'in antiphase', 'S lagging N']
                for bctr in range(4):
                    if ttt == 'avg':
                        KR_MIN = 2
                        KR_MAX = 15
                        levels = np.linspace(np.log10(KR_MIN),np.log10(KR_MAX),num=11)
                    elif ttt == 'med':
                        KR_MIN = 2
                        KR_MAX = 8
                        levels = np.linspace(np.log10(KR_MIN),np.log10(KR_MAX),num=11)
                    ax = axes[bctr]
                    if ttt=='avg':
                        data = np.copy(beatavgs[bctr,:,:])
                    else:
                        data = np.copy(beatmeds[bctr,:,:])
                    data = np.log10(data)
#                    data = data/np.nanmax(data)
                    data[np.where(data<=0)] = 0.000001
                    theta = np.linspace(0,2*np.pi,num=np.shape(image)[0]+1,endpoint=True)
                    r = np.linspace(0,30,num=np.shape(image)[1]+1,endpoint=True)
                    # pcolormesh
        #            quad = ax.pcolormesh(theta, r, data.T, cmap=cmap_UV)
        #            quad.set_norm(colors.LogNorm(KR_MIN,KR_MAX))
                    # contourf
                    theta = theta[:-1]+np.diff(theta)/2
                    r = r[:-1]+np.diff(r)/2
                    quad = ax.contourf(theta,r,data.T,levels,cmap=cmap_UV,extend='both')
                    quad.cmap.set_under('k')
        #            quad.cmap.set_over('orangered')
                    quad.cmap.set_over('w')  
                    
                    ax.set_theta_zero_location('S')
                    ax.set_theta_direction(-1)
                    ax.set_rlim([0,30])
                    
                    xtic = np.arange(0,2*np.pi,np.pi/6)
                    xlab = ['$\Psi_\mathrm{N}=%s^\mathrm{o}$' % int(xtic[iii]*180/np.pi)
                        if not iii%3 else '' for iii in range(len(xtic))]
                    ax.set_xticks(xtic)
                    ax.set_xticklabels([])
                    for iii in range(len(xtic)):
                        ax.text(xtic[iii], 32, xlab[iii], color='k',
                                fontsize=12, horizontalalignment='center', verticalalignment='center',
                                rotation=xtic[iii]*180/np.pi if iii%6 else 'horizontal')
                    
                    ax.set_yticks([10,20])
                    ax.set_yticklabels(['',''])
                    ax.grid(color='0.8',linestyle='--', linewidth=1)
                    ax.set_title('%s (%d)' % (titles[bctr], save_nums[bctr+1]), y=1.06, fontweight='bold', fontsize=12)
                    ax.text(0,1,subf[bctr], transform=ax.transAxes, color='k', fontsize=16, fontweight='bold')
                    
                markRegion(ax1, 30, 150)
                markRegion(ax3, 30, 60)
                markRegion(ax4, 120, 150)
                    
                cbar = plt.colorbar(quad, cax=cax, extend='both')
                cbar.set_label('Intensity (kR)', rotation=270, labelpad=20, fontsize=12)
                ticks = np.arange(KR_MIN,16)
                cbar.set_ticks(np.log10(ticks))
                cbar.set_ticklabels(ticks)
                
                fn = 'z_beat_{}'.format(ttt)
                plt.savefig('%s/%s.png' %  (fullsavepath,fn),
                            bbox_inches='tight', dpi=500)
                plt.savefig('%s/%s.pdf' %  (fullsavepath,fn),
                            bbox_inches='tight', dpi=500)
                plt.show()
                plt.close()
                
                
            # check how many different days are included
            with open('%s/coverage.txt' % fullsavepath, 'w') as f:
                for iii in range(1,5):
                    print('=======================\n=======================', file=f)
                    print(beatphase[iii], file=f)
                    tmp = imglist[(imglist['HEMISPHERE']=='North') &
                            (imglist['PPO_BEAT_PHASE'] == iii) & 
                            (imglist['SUBINT_outside_coalescence'] == 1)].groupby(by=['YEAR','DOY']).count()['EXP']
                    print(tmp, file=f)
                    print('SUM:', tmp.sum(), file=f)
                                    
                
#        # SKR intensity histogram
#        skrref = ['SKR_N', 'SKR_S']
#        stat_min = [10,1,1,1]
#        stat_max = [60,10,8,15]
#        for skrctr in range(len(skrref)):
#            for hemctr in range(len(hems)):
#                tmp = np.digitize(skr_angles[:,skrctr], histlonbins/180*np.pi)-1
#                tmp[np.where(np.isnan(skr_angles[:,skrctr]))] = 99
#                tmp[np.where(imagelist['HEMISPHERE'] != hems[hemctr])] = 99
#                stat = np.full((len(histlonbins)-1, np.shape(img_intensities)[1]), np.nan)
#                for iii in range(len(histlonbins)-1):
#                    for jjj in range(np.shape(img_intensities)[1]):
#                        stat[iii,jjj] = np.nanmean(img_intensities[np.where(tmp==iii)[0],jjj])
#                angcent = histlonbins[:-1] + np.diff(histlonbins)/2
#                for iii in range(np.shape(img_intensities)[1]):
#                    fig = plt.figure()
#                    ax = fig.add_subplot(111)
#                    ax.bar(angcent, stat[:,iii],width=np.mean(np.diff(angcent))*0.8)
#                    ax.set_xlabel('{} phase (deg)'.format(skrref[skrctr]))
#                    ax.set_ylabel('Median {} intensity (kR)'.format(stat_names[iii]))
#                    ax.set_xticks([0,90,180,270,360])
#                    ax.set_xlim([0,360])
#                    ax.set_yscale('log')
#                    ax.set_ylim([stat_min[iii], stat_max[iii]])
#                    ax.set_title('Intensity statistics for {}ern hemisphere, {}'.format(hems[hemctr], skrref[skrctr]))
#                    plt.savefig('{}/skr_stat_{}_{}_{}.png'.format(fullsavepath,stat_names[iii],hems[hemctr],skrref[skrctr]),
#                                bbox_inches='tight', dpi=500)
#                    plt.show()
#                    plt.close()