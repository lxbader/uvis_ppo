# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)

import matplotlib.pyplot as plt
import numpy as np
import os
import time

# path to list of UVIS images
listpath = '%s/UVIS/UVISlist_RES2.npz' % datapath
imagelist = np.load(listpath)['data'][()]

# save path
savepath = '{}/Plots/Double_peaks/{}'.format(boxpath,time.strftime('%Y%m%d%H%M%S'))
if not os.path.exists(savepath):
    os.makedirs(savepath)

# path to file where fit centers and boundaries are stored, if calculated already
centerpath = '%s/imglists/UVIS_cent_bound_fwhm_double_MA25_MR30.npz' % boxpath
# load them if available
doublepeaks = np.load(centerpath)['doublepeaks'][()]
loncenters = np.load(centerpath)['loncenters'][()]
enoughdata = np.load(centerpath)['enoughdata'][()]
print('Double peak data loaded')

hems = ['North','South']
for hem in hems:
    valid = np.where(imagelist['HEMISPHERE'] == hem)[0]
    
    dpk = doublepeaks[valid,:,:]
    edt = enoughdata[valid,:]
    ppo_n= imagelist['PPO_N_S'][valid,0]*180/np.pi
    ppo_s= imagelist['PPO_N_S'][valid,1]*180/np.pi
    
    dpk_ppo_n = np.copy(dpk)
    dpk_ppo_s = np.copy(dpk)
    edt_ppo_n = np.copy(edt)
    edt_ppo_s = np.copy(edt)
    
    for iii in range(len(dpk[:,0,0])):
        dpk_ppo_n[iii,:,:] = np.roll(np.flip(dpk[iii,:,:],axis=-1),
                                 np.around((ppo_n[iii]/360*len(loncenters))).astype(int),
                                 axis=-1)
        dpk_ppo_s[iii,:,:] = np.roll(np.flip(dpk[iii,:,:],axis=-1),
                                 np.around((ppo_s[iii]/360*len(loncenters))).astype(int),
                                 axis=-1)
        edt_ppo_n[iii,:] = np.roll(np.flip(edt[iii,:],axis=-1),
                                 np.around((ppo_n[iii]/360*len(loncenters))).astype(int),
                                 axis=-1)
        edt_ppo_s[iii,:] = np.roll(np.flip(edt[iii,:],axis=-1),
                                 np.around((ppo_s[iii]/360*len(loncenters))).astype(int),
                                 axis=-1)
        
    refsys = ['LT','PPO_N','PPO_S']
    hist_LT = np.nansum(np.nansum(dpk,axis=1)>0, axis=0)/np.nansum(edt,axis=0)
    hist_PPO_N = np.nansum(np.nansum(dpk_ppo_n,axis=1)>0, axis=0)/np.nansum(edt_ppo_n,axis=0)
    hist_PPO_S = np.nansum(np.nansum(dpk_ppo_s,axis=1)>0, axis=0)/np.nansum(edt_ppo_s,axis=0)
    histsys = [hist_LT,hist_PPO_N,hist_PPO_S]
    
    fig = plt.figure()
    ax = fig.add_subplot(111)
    for refctr in range(len(refsys)):
        ax.plot(loncenters, histsys[refctr], label=refsys[refctr])
    ax.legend()
    ax.set_xlim([0,360])
    ax.set_xticks(np.arange(0,361,90))
    ax.set_xticklabels(['{} LT/{}$^o$'.format(int(iii/15),iii) for iii in np.arange(0,361,90)])
    ax.set_xlabel('Longitude (LT/$\psi$)')
    ax.set_ylabel('Number of double peaks / number of available sections')
    ax.set_title('{}ern hemisphere'.format(hem))
    ax.grid()
    
    plt.savefig('{}/{}.png'.format(savepath,hem),
                    bbox_inches='tight', dpi=500)
    plt.show()
    
    




