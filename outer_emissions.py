# plot HST/UVIS files
import path_register
pr = path_register.PathRegister()
gitpath = pr.gitpath
datapath = pr.datapath
plotpath = pr.plotpath
uvispath = pr.uvispath

import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import get_image
import numpy as np
import os
import time
import plot_HST_UVIS
import uvisdb

udb = uvisdb.UVISDB()
imglist = udb.currentDF

def filterScatter(values, windowsize):
    filtered = np.full_like(values, np.nan)
    for jjj in range(len(filtered)):
        if jjj<len(filtered)/2:
            tmp = np.roll(values, windowsize)
            argmin = jjj
            argmax = jjj+2*windowsize+1
        else:
            tmp = np.roll(values, -windowsize)
            argmin = jjj-2*windowsize
            argmax = jjj+1
        filtered[jjj] = np.nanmean(tmp[argmin:argmax])
    return filtered

savepath = '{}/secondary_emissions/{}'.format(plotpath, time.strftime('%Y%m%d%H%M%S'))
if not os.path.exists(savepath):
    os.makedirs(savepath)
        
# make some average images
refs = ['LT','PPON','PPOS']
hems = ['North','South']
for hem in hems:
    print(hem)
    selec = imglist[(imglist['HEMISPHERE']==hem) & (imglist['POS_KRTP_R']<15) &
                    (np.isfinite(imglist['PPO_PHASE_N'])) & (np.isfinite(imglist['PPO_PHASE_S']))]
    if not len(selec):
        continue
    tmp, _ = get_image.getRedUVIS(uvispath+selec['FILEPATH'].iloc[0])
    images = np.full((3,len(selec),) + np.shape(tmp), np.nan)
    for iii in range(len(selec)):
        if not iii % 50:
            print(iii, '/', len(selec))
        for rrr in range(len(refs)):
            ref = refs[rrr]
            name = selec['FILEPATH'].iloc[iii].strip('.fits').split('/')[-1].split('\\')[-1]
            if ref == 'LT':
                thisimg, _ = get_image.getRedUVIS(uvispath+selec['FILEPATH'].iloc[iii])
            elif ref == 'PPON':
                thisimg = np.load(uvispath+'/PROJECTIONS_ROT_FLIP/PPON/' + name + '.npz')['rot_img']
            elif ref == 'PPOS':
                thisimg = np.load(uvispath+'/PROJECTIONS_ROT_FLIP/PPOS/' + name + '.npz')['rot_img']
            images[rrr,iii,:,:] = thisimg
    
    for rrr in range(len(refs)):
        plot_HST_UVIS.simpleplot(np.nanmedian(images[rrr,:,:,:], axis=0),
                                 '{}/median_{}_{}'.format(savepath,hem,refs[rrr]),
                                 '{}, {} ({} images)'.format(hem, refs[rrr], len(selec)),
                                 style='PPO' if rrr>0 else 'LT', KR_MIN=1, KR_MAX=10, nlev=12)
        plot_HST_UVIS.simpleplot(np.nanmean(images[rrr,:,:,:], axis=0),
                                 '{}/mean_{}_{}'.format(savepath,hem,refs[rrr]),
                                 '{}, {} ({} images)'.format(hem, refs[rrr], len(selec)),
                                 style='PPO' if rrr>0 else 'LT', KR_MIN=1, KR_MAX=10, nlev=12)
sys.exit()
    
refs = ['PPON','PPOS']
for hem in hems:
    print(hem)
    selec = imglist[(imglist['HEMISPHERE']==hem) & (np.isfinite(imglist['PPO_PHASE_N'])) 
                        & (np.isfinite(imglist['PPO_PHASE_S'])) & (imglist['BOUNDARIES'].apply(np.nansum)>0)
                        & (imglist['DOUBLEPEAKS'].apply(np.nansum)>0)]
    if not len(selec):
        continue
    tmp, _ = get_image.getRedUVIS(uvispath+selec['FILEPATH'].iloc[0])
    latbins = np.linspace(0,30,num=np.shape(tmp)[-1]+1)
    latbins = latbins[:-1]+np.diff(latbins)/2
    outer_img = np.full((2,len(selec),) + np.shape(tmp), np.nan)
    for iii in range(len(selec)):
        if not iii % 50:
            print(iii, '/', len(selec))
        for rrr in range(len(refs)):
            ref = refs[rrr]
            name = selec['FILEPATH'].iloc[iii].strip('.fits').split('/')[-1].split('\\')[-1]
            if ref == 'PPON':
                thisimg = np.load(uvispath+'/PROJECTIONS_ROT_FLIP/PPON/' + name + '.npz')['rot_img']
            elif ref == 'PPOS':
                thisimg = np.load(uvispath+'/PROJECTIONS_ROT_FLIP/PPOS/' + name + '.npz')['rot_img']
                
            ppo = selec['PPO_PHASE_N' if ref=='PPON' else 'PPO_PHASE_S'].iloc[iii]
            bounds = selec['BOUNDARIES'].iloc[iii]
            clean_outer = np.repeat(filterScatter(bounds[-1,:], 2),
                                    int(np.shape(thisimg)[0]/np.shape(bounds)[-1]))
            shift = -int(np.floor(len(clean_outer)*ppo/(2*np.pi)))
            ppobounds = np.roll(clean_outer, shift)[::-1]
            
            test = np.zeros_like(thisimg)
            for jjj in range(np.shape(outer_img)[-2]):
                tmp = np.where(latbins>ppobounds[jjj]+2)[0]
                test[jjj,tmp] = 1
                outer_img[rrr,iii,jjj,tmp] = thisimg[jjj,tmp]
                
#            plt.pcolormesh(np.log10(thisimg.T))
#            plt.show()
#            plt.plot(ppobounds.T)
#            plt.show()
#            plt.pcolormesh(np.log10(outer_img[rrr,iii,:,:].T))
#            plt.show()
    sys.exit()        
    for rrr in range(len(refs)):
        MINNUM = 10
        num = np.sum(np.isfinite(outer_img[rrr,:,:,:]), axis=0)
        data = np.nanmedian(outer_img[rrr,:,:,:], axis=0)
        data[np.where(num<MINNUM)] = np.nan
        plot_HST_UVIS.simpleplot(data,
                                 '{}/outer_median_{}_{}'.format(savepath,hem,refs[rrr]),
                                 '{}, {} ({} images)'.format(hem, refs[rrr], len(selec)),
                                 style='PPO' if rrr>0 else 'LT', KR_MIN=1, KR_MAX=10, nlev=12)
        data = np.nanmean(outer_img[rrr,:,:,:], axis=0)
        data[np.where(num<MINNUM)] = np.nan
        plot_HST_UVIS.simpleplot(data,
                                 '{}/outer_mean_{}_{}'.format(savepath,hem,refs[rrr]),
                                 '{}, {} ({} images)'.format(hem, refs[rrr], len(selec)),
                                 style='PPO' if rrr>0 else 'LT', KR_MIN=1, KR_MAX=10, nlev=12)
            
                            
        

# delete data within the outer boundary of the auroral oval
rot_img_outer = np.copy(rot_img)
for iii in range(np.shape(rot_img_outer)[0]):
    if not iii % 100:
        print(iii,'/',np.shape(rot_img_outer)[0])
    for jjj in range(np.shape(rot_img_outer)[1]):
        for kkk in range(np.shape(rot_img_outer)[2]):
            if not np.isnan(clean_outer[iii,jjj,kkk]):
                tmp = np.where(latbins<=clean_outer[iii,jjj,kkk]+2)[0]
                rot_img_outer[iii,jjj,kkk,tmp] = np.nan
            else:
                rot_img_outer[iii,jjj,kkk,:] = np.nan
                
hems = ['North','South']
for hemctr in range(len(hems)):
    thishem = hems[hemctr]
    arg = np.where((imglist['HEMISPHERE'] == thishem)*
                   (imglist['POS_KRTP'][:,0] <30)*
                   (np.nansum(rot_img_outer[:,hemctr,:,:], axis=(-2,-1)))
                   )[0]
    image = np.nanmedian(rot_img_outer[arg,hemctr+1,:,:], axis=0)

    plot_HST_UVIS.simpleplot(image[::-1,:], '{}/average_{}_outer'.format(savepath,thishem), thishem+', {} images'.format(len(arg)),
                             style='PPO', KR_MIN=0.1 if hemctr==0 else 0.1, KR_MAX=4 if hemctr==0 else 4, nlev=12)
        
