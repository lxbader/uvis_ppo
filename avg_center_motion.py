# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
boxpath = pr.boxpath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cubehelix
from datetime import datetime
import matplotlib as mpl
import matplotlib.gridspec as gridspec
from matplotlib.collections import LineCollection
import matplotlib.pyplot as plt
import numpy as np
import os
import scipy.optimize as sopt
import time
import time_conversions as tc
import uvisdb

udb = uvisdb.UVISDB()
imglist = udb.currentDF.copy()

#centers = np.array(imglist[['CENTER_LON','CENTER_COLAT','CENTER_X','CENTER_Y','CENTER_STAT']])
centers = np.array(imglist[['CIRCLE_LON','CIRCLE_COLAT',
                            'CIRCLE_X','CIRCLE_Y','CIRCLE_STAT']])



#imglist[(np.abs(imglist['POS_KRTP_LAT']) > 45)].groupby(by=['HEMISPHERE'])['POS_KRTP_R'].plot(kind='hist')

# determine beat phase for all images        
# calculate PPO beat phase
imglist['PPO_SminN'] = (imglist['PPO_PHASE_S'] - imglist['PPO_PHASE_N']) % (2*np.pi)
imglist['PPO_BEAT_PHASE'] = 0
# in phase
imglist['PPO_BEAT_PHASE'][(imglist['PPO_SminN']<=1/4*np.pi) |
                            (imglist['PPO_SminN']>7/4*np.pi)] = 1
# S leading N
imglist['PPO_BEAT_PHASE'][(imglist['PPO_SminN']>1/4*np.pi) &
                            (imglist['PPO_SminN']<=3/4*np.pi)] = 2
# antiphase
imglist['PPO_BEAT_PHASE'][(imglist['PPO_SminN']>3/4*np.pi) &
                            (imglist['PPO_SminN']<=5/4*np.pi)] = 3
# S lagging N
imglist['PPO_BEAT_PHASE'][(imglist['PPO_SminN']>5/4*np.pi) &
                            (imglist['PPO_SminN']<=7/4*np.pi)] = 4
       
beatphase = ['00all','01inphase','02SaheadN','03antiphase','04SbehindN']
       

cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)
cmap_SPEC = cubehelix.cmap(reverse=False, start=1.8, rot=1, gamma=1.5)

# define trigonometric function with several free parameters
def freeTrig(x,ampl,phaseshift,yoffset):
    return ampl*np.sin((x+phaseshift)/180*np.pi)+yoffset

#===================================================
# fitting and plotting the ellipse fit center motion

# path for saving plots
mainsavepath = '%s/Plots/PPO_ellipse/%s' % (boxpath, time.strftime('%Y%m%d%H%M%S'))
if not os.path.exists(mainsavepath):
    os.makedirs(mainsavepath)

# define sub-intervals of the Cassini mission
datefmt = '%Y-%jT%H-%M-%S'
subinterv = {}
subinterv['all'] = ['2007-001T00-00-00', '2017-258T23-59-59']
subinterv['before'] = ['2005-181T00-00-00', '2013-180T23-59-59']
subinterv['during'] = ['2013-181T00-00-00', '2014-180T23-59-59']
subinterv['after'] = ['2014-181T00-00-00', '2017-258T23-59-59']
subintervnames = np.array([*subinterv])
for iii in [*subinterv]:
    tmp = [datetime.strptime(subinterv[iii][jjj], datefmt)
            for jjj in range(len(subinterv[iii]))]
    subinterv[iii] = tc.datetime2et(tmp)

for bctr in range(len(beatphase)):
    # save some fit parameters and stuff for later
    phispace = np.linspace(0,360,num=361)
    xplot = np.zeros((2,2,len(phispace)))
    yplot = np.zeros((2,2,len(phispace)))
    xopt = np.zeros((2,2,3))
    yopt = np.zeros((2,2,3))
    
    savepath = '{}/{}'.format(mainsavepath, beatphase[bctr])
    if not os.path.exists(savepath):
        os.makedirs(savepath)
    
    
    for hemctr in range(2):
        hem = ['North','South'][hemctr]
        # sort out interesting data (right hemisphere, good ellipse fit quality)
        if bctr>0:
            argdata = np.where((imglist['HEMISPHERE'] == hem) & 
                               (centers[:,4]<0.5) & 
                               (imglist['PPO_BEAT_PHASE'] == bctr))[0]
        else:
            argdata = np.where((imglist['HEMISPHERE'] == hem) & 
                               (centers[:,4]<1))[0]
            
        for refctr in range(2):
            reflist = ['PPO_N','PPO_S']
            refmath = ['\Phi_\mathrm{N}','\Phi_\mathrm{S}']
            ref = reflist[refctr]
            print(hem,ref)
                    
            # get phase angle of respective reference system for selected images
            # always referenced to 0 LT / 0 deg lon / midnight in CCW direction
            if ref=='PPO_N':
                refangle = imglist['PPO_PHASE_N'].iloc[argdata]+np.pi
            elif ref=='PPO_S':
                refangle = imglist['PPO_PHASE_S'].iloc[argdata]+np.pi
            refangle = refangle % (2*np.pi)
            refangle = refangle*180/np.pi
            
            # get time of each image
            times = imglist[['ET_START','ET_STOP']].iloc[argdata].mean(axis=1)
            # get fitted centers
            centerdata = centers[argdata,:]
            
            # set up figure
            fig = plt.figure()
            fig.set_size_inches(10,7)
            gs1 = gridspec.GridSpec(2, 1)
    #        gs1.update(left=0, right=0.65)
            ax1 = plt.subplot(gs1[0, 0])
            ax2 = plt.subplot(gs1[1, 0])
    #        gs2 = gridspec.GridSpec(2, 1)
    #        gs2.update(left=0.70, right=1)
    #        bx1 = plt.subplot(gs2[0, 0])
    #        bx2 = plt.subplot(gs2[1, 0])
            
            # define color succession for different sub-intervals
            cls = ['k', 'r', 'g', 'b']
            colorlist = iter(cls)
            cmaps = iter(['Greys', 'Reds', 'Greens', 'Blues'])
            
            # loop through these intervals
            subintervnames_selec = ['all']
    #        for sctr in subintervnames:
            for sctr in subintervnames_selec:
                ccc = next(colorlist)
                cccm = next(cmaps)
                # select right image times and get data
                valtimes = np.where((times>subinterv[sctr][0])*
                                    (times<subinterv[sctr][1]))[0]
                # exclude coalescence period
                if sctr == 'all':
                    valtimes2 = np.where(np.logical_or(times<subinterv['during'][0],
                                                      times>subinterv['during'][1]))[0]
                    valtimes = np.intersect1d(valtimes, valtimes2)
                    print(hem, ref, len(valtimes))
                    
                tmptimes = times.iloc[valtimes]
                tmprefangle = refangle.iloc[valtimes]
                tmpxdata = -centerdata[valtimes,2]
                tmpydata = -centerdata[valtimes,3]
                                        
                # for displacements in x and y directions
                for ax in [ax1,ax2]:
                    if ax==ax1:
                        data = tmpxdata
                    elif ax==ax2:
                        data = tmpydata
                        
                    # find valid values and fit a sine to them if possible
                    val = np.intersect1d(np.where(np.isfinite(tmprefangle))[0],
                                         np.where(np.isfinite(data))[0])
                    if sctr=='all':
                        histtimes = np.copy(tmptimes.iloc[val])
                    
                    if np.size(val)>5:
                        val = val[np.argsort(tmprefangle.iloc[val])]
                        opt, cov = sopt.curve_fit(freeTrig, tmprefangle.iloc[val], data[val], p0=(3,0,0))
                        if opt[0] < 0:
                            opt[0] = np.abs(opt[0])
                            shift = opt[1]+180
                            if shift > 180:
                                shift = shift-360
                            opt[1] = shift
                        phisp = np.linspace(0,360,num=1000,endpoint=True)
                        if ref=='PPO_N':
                            ax.plot(phisp, freeTrig(phisp,opt[0],opt[1],opt[2]), color=ccc, label=r'$%.1f*sin(\Phi_\mathrm{N}%+.1f)%+.1f$' % (opt[0], opt[1], opt[2]))
                        else:
                            ax.plot(phisp, freeTrig(phisp,opt[0],opt[1],opt[2]), color=ccc, label=r'$%.1f*sin(\Phi_\mathrm{S}%+.1f)%+.1f$' % (opt[0], opt[1], opt[2]))
                        if hem=='North' and ref=='PPO_N':                        
                            if ax==ax1:
                                ax.plot(phisp, freeTrig(phisp,0.5,66,-1.3), color='b', label=r'$0.5*sin(\Phi_\mathrm{N}+66)-1.3$ (Nichols 2016)')
                            else:
                                ax.plot(phisp, freeTrig(phisp,1.6,-41,-0.9), color='b', label=r'$1.6*sin(\Phi_\mathrm{N}-41)-0.9$ (Nichols 2016)')
                        if hem=='South' and ref=='PPO_S':                        
                            if ax==ax1:
                                ax.plot(phisp, freeTrig(phisp,1.8,-357,-1.7), color='b', label=r'$1.8*sin(\Phi_\mathrm{S}-357)-1.7$ (Nichols 2008)')
                            else:
                                ax.plot(phisp, freeTrig(phisp,1.3,202,-1.4), color='b', label=r'$1.3*sin(\Phi_\mathrm{S}+202)-1.4$ (Nichols 2008)')
                                
                        ax.scatter(tmprefangle, data, marker='+', color=ccc, edgecolor='none', s=120, linewidth=0.5)
    #                    ax.errorbar(tmprefangle, data, xerr=xerr, fmt='o')
                        
                        if ax == ax1:
                            xplot[hemctr,refctr,:] = freeTrig(phispace,opt[0],opt[1],opt[2])
                            xopt[hemctr,refctr,:] = opt
                        elif ax == ax2:
                            yplot[hemctr,refctr,:] = freeTrig(phispace,opt[0],opt[1],opt[2])
                            yopt[hemctr,refctr,:] = opt
                                            
                        # for the full mission, find and plot standard deviations
                        if sctr == 'all':
                            stddev = np.sqrt(np.diag(cov))
                            stdmin = freeTrig(phisp,opt[0],opt[1],opt[2])
                            stdmax = freeTrig(phisp,opt[0],opt[1],opt[2])
                            for iii in [-1,0,1]:
                                for jjj in [-1,0,1]:
                                    for kkk in [-1,0,1]:
                                        tmp = freeTrig(phisp,opt[0]+iii*stddev[0],opt[1]+iii*stddev[1],opt[2]+iii*stddev[2])
                                        less = np.where(tmp<stdmin)[0]
                                        stdmin[less] = tmp[less]
                                        bigger = np.where(tmp>stdmax)[0]
                                        stdmax[bigger] = tmp[bigger]
                            # mark grey area for standard deviation
                            ax.fill_between(phisp, stdmax, stdmin, facecolor='grey', alpha=0.5)
                            # mark phi angle with maximum displacement and its stdev
                            vmax = (90-opt[1]) % 360
                            ax.axvline(vmax, color='k', linestyle='--')
                            ax.fill_betweenx([-10,10],[vmax-stddev[1],vmax-stddev[1]],[vmax+stddev[1],vmax+stddev[1]],facecolor='grey', alpha=0.5)
                            ax.fill_betweenx([-10,10],[vmax-stddev[1],vmax-stddev[1]],[vmax+stddev[1],vmax+stddev[1]],facecolor='grey', alpha=0.5)
                            
                    else:
                        # plot data points
                        ax.scatter(tmprefangle, tmpxdata, marker='+', label=sctr, color=ccc)
                    
    #                # plot center motion in polar view
    #                if np.any(xplot) and np.any(yplot):
    #                    if sctr == 'all':
    #                        bx1.scatter(yplot,-xplot,c=np.arange(len(xplot)), cmap=cccm, label=sctr)
    #                    else:
    #                        bx2.scatter(yplot,-xplot,c=np.arange(len(xplot)), cmap=cccm, label=sctr)
            # some general figure setup
            ax1.set_xlabel('$%s$ (deg)' % refmath[refctr])
            ax1.set_ylabel('x-displacement (midnight/noon -/+)')
            ax1.set_xlim([0,360])
            ax1.set_ylim([-5,5])
            ax1.legend(loc=1)
            ax1.text(0.01, 0.9, '(a)', transform=ax1.transAxes, fontweight='bold', fontsize=15)
    
            ax2.set_xlabel('$%s$ (deg)' % refmath[refctr])
            ax2.set_ylabel('y-displacement (dawn/dusk -/+)')
            ax2.set_xlim([0,360])
            ax2.set_ylim([-5,5])
            ax2.legend(loc=1)
            ax2.text(0.01, 0.9, '(b)', transform=ax2.transAxes, fontweight='bold', fontsize=15)
            
    #        bx1.grid()
    #        bx1.axis('equal')
    #        bx1.scatter(0,0,s=100, marker='+', color='k')
    #        bx1.set_xlim([-4,4])
    #        bx1.set_ylim([-4,4])
    #        bx1.set_xlabel('Dawn-dusk displacement [deg colatitude]')
    #        bx1.set_ylabel('Noon-midnight displacement [deg colatitude]')
    #        bx1.legend(loc=3)
    #        leg = bx1.get_legend()
    #        leg.legendHandles[0].set_color('k')
    #        
    #        bx2.grid()
    #        bx2.axis('equal')
    #        bx2.scatter(0,0,s=100, marker='+', color='k')
    #        bx2.set_xlim([-4,4])
    #        bx2.set_ylim([-4,4])
    #        bx2.set_xlabel('Dawn-dusk displacement [deg colatitude]')
    #        bx2.set_ylabel('Noon-midnight displacement [deg colatitude]')
    #        leg = bx2.legend(loc=3)
    #        colorlist = iter(cls)
    #        for iii in subintervnames_selec:
    #            ccc = next(colorlist)
    #            for jjj in range(len(leg.texts)):
    #                if leg.texts[jjj].get_text() == iii:
    #                    leg.legendHandles[jjj].set_color(ccc)
            
    #        plt.suptitle('%sern hemisphere ellipse fit center displacement in relation to %s reference frame'
    #                     % (hem, ref.replace('_',' ')), x=0.5, y=0.92)
            plt.savefig('%s/%s_%s_displacement.png' % (savepath,hem,ref),
                    bbox_inches='tight', dpi=500)
            plt.savefig('%s/%s_%s_displacement.pdf' % (savepath,hem,ref),
                    bbox_inches='tight', dpi=500)
            plt.show()
            plt.close()
            
#            # make histograms for number of images
#            years = np.arange(2005,2019)
#            dtbins = np.array([datetime(iii,1,1) for iii in years])
#            etbins = tc.datetime2et(dtbins)
#            etcenters = etbins[:-1]+np.diff(etbins)/2
#            hist, trash = np.histogram(histtimes,etbins)
#            fig = plt.figure()
#            ax = fig.add_subplot(111)
#            ax.bar(etcenters,hist,width=np.mean(np.diff(etbins))*0.8,color='b')
#            ax.set_xlabel('Year')
#            ax.set_ylabel('Number of UVIS images')
#            ax.set_xticks(etcenters)
#            ax.set_xticklabels(years[:-1],rotation=45)
#            ax.set_title('Number of ellipse fits, %s, %s' % (hem,ref.replace('_',' ')))
#            plt.savefig('%s/%s_%s_number.png' % (savepath,hem,ref),
#                            bbox_inches='tight', dpi=500)
#            plt.savefig('%s/%s_%s_number.pdf' % (savepath,hem,ref),
#                            bbox_inches='tight', dpi=500)
#            plt.show()
#            plt.close()
            
    
    def add_subplot_axes(ax,rect):
        fig = plt.gcf()
        box = ax.get_position()
        width = box.width
        height = box.height
        inax_position  = ax.transAxes.transform(rect[0:2])
        transFigure = fig.transFigure.inverted()
        infig_position = transFigure.transform(inax_position)    
        x = infig_position[0]
        y = infig_position[1]
        width *= rect[2]
        height *= rect[3]  # <= Typo was here
        subax = fig.add_axes([x,y,width,height], projection='polar')
        x_labelsize = subax.get_xticklabels()[0].get_size()
        y_labelsize = subax.get_yticklabels()[0].get_size()
        x_labelsize *= rect[2]**0.5
        y_labelsize *= rect[3]**0.5
        subax.xaxis.set_tick_params(labelsize=x_labelsize)
        subax.yaxis.set_tick_params(labelsize=y_labelsize)
        return subax
    
    
    fig = plt.figure()
    fig.set_size_inches(9,6)
    #fig.set_figwidth(12)
    ax1 = fig.add_subplot(121, aspect='equal')
    ax2 = fig.add_subplot(122, aspect='equal', sharex=ax1, sharey=ax1)
    
    for axctr in range(2):
        ax = [ax1,ax2][axctr]
        hctr = [0,1][axctr]
        rctr = [0,1][axctr]
        
        t = np.linspace(1,0,num=len(xplot[0,0,:]))
        x = yplot[hctr,rctr,:]
        y = -xplot[hctr,rctr,:]
        points = np.array([x, y]).T.reshape(-1, 1, 2)
        segments = np.concatenate([points[:-1], points[1:]], axis=1)
        
        lc = LineCollection(segments, cmap=cmap_SPEC,
                            norm=plt.Normalize(0, 1))
        lc.set_array(t)
        lc.set_linewidth(7)
        ax.add_collection(lc)
        
        ax.plot(x,y,c='none')
    
        ax.scatter(yopt[hctr,rctr,2], -xopt[hctr,rctr,2], marker='+',
                   s=100, linewidth=0.5, color='k')
        
        angles = np.arctan2(np.diff(y), np.diff(x))*180/np.pi
        angles = np.append(angles, [angles[-1]])
        
        # mark certain PPO angles
        ppoangles = [0,45,90,135,180,225,270,315]
        for iii in range(len(ppoangles)):
            amin = np.argmin(np.abs(ppoangles[iii]-phispace))
            t = mpl.markers.MarkerStyle(marker='|')
            t._transform = t.get_transform().rotate_deg(angles[amin])
            ax.scatter(x[amin], y[amin], marker=t, linewidth=1.5,
                       s=200, c='k', zorder=5)
            textangle = (angles[amin]-90)/180*np.pi
            [xoffs, yoffs] = np.array([np.cos(textangle), np.sin(textangle)])*0.2
            if iii==0:
                tmptxt = '$\Phi_\mathrm{%s}=%d^\mathrm{o}$' % ('N' if not axctr else 'S', ppoangles[iii]) 
            else:
                tmptxt = '$%d^\mathrm{o}$' % (ppoangles[iii])
            ax.annotate(tmptxt,
                        xy=(x[amin],y[amin]), xytext=(x[amin]+xoffs,y[amin]+yoffs),
                        ha='center', va='center')
            
        # calculate Psi range
        xdiff = xplot[hctr,rctr,:]-xopt[hctr,rctr,2]
        ydiff = yplot[hctr,rctr,:]-yopt[hctr,rctr,2]
        tmpangle = np.arctan2(ydiff,xdiff)*180/np.pi % 360
        psispace = (phispace-tmpangle) % 360
        psispace[np.where(psispace>300)] -= 360
        ax.text(0.80,0.08, 'Displacement\n direction:\n $\Psi_\mathrm{%s}=%d-%d^\mathrm{o}$' % (
                'N' if not axctr else 'S', int(np.min(psispace) if np.min(psispace)>0 else np.min(psispace)+360), int(np.max(psispace))),
            ha='center', va='center',
            transform=ax.transAxes,
            bbox={'facecolor':'0.7', 'pad':5})
        
        ax.axvline(x=0, ymin=0, ymax=1, c='k', lw=1, ls='--')
        ax.axhline(y=0, xmin=0, xmax=1, c='k', lw=1, ls='--')
        ax.grid()
        ax.set_title('North' if not axctr else 'South')
        
        ax.set_xlim([-2.5,1])
        ax.set_ylim([-0.5,3.5])
        
        ax.set_xlabel('Dawn - dusk colatitude (deg)')
        
        ax.text(0.05,0.95,'(a)' if not axctr else '(b)', transform=ax.transAxes, fontweight='bold',
                fontsize=15)
        
        
    ax1.set_ylabel('Noon - midnight colatitude (deg)')
    plt.setp(ax2.get_yticklabels(), visible=False)
    plt.subplots_adjust(wspace=0.05)
    
    #ax3 = plt.axes([0.2,0.55,0.3,0.3], projection='polar', transform=ax1.transAxes)
    ax3 = add_subplot_axes(ax1, [0.15,0.48,0.5,0.5])
    ax3.set_theta_zero_location('S')
    ax3.set_xticks([0,np.pi/2,np.pi,3*np.pi/2])
    ax3.set_xticklabels(['12 LT', '18 LT', '0 LT', '6 LT'], fontsize=10)
    ax3.set_yticks([])
    ax3.set_ylim([0,1])
    ppoangles = [45,90,135,180,225,270,315]
    offsets = [0,0,-2,-2,1,0,+2]
    for ctr in range(len(ppoangles)):
        iii = ppoangles[ctr]
        width = 25
        xs = np.linspace(iii-width/2, iii+width/2, num=20)*np.pi/180
        ax3.fill_between(xs,0,1,color=cmap_SPEC(1-iii/360))
        pos = (iii+offsets[ctr])/180*np.pi 
        ax3.text(pos,0.65,'$%d^\mathrm{o}$' % (iii),
                 ha='center', va='center',
                 rotation=iii-90 if iii-90<=90 else iii+90,
                 color='k' if iii<180 else 'w')
    ax3.grid('off')
    
    ax3.text(359*np.pi/180,0.65,'$\Phi=0^\mathrm{o}$',
                 ha='center', va='center',
                 rotation=-90, color='k')
    
    #num = 31
    #xs1 = np.linspace(360-width/2,360, num=20)*np.pi/180
    #xs2 = np.linspace(0, width/2, num=20)*np.pi/180
    #for nnn in range(num):
    #    ax3.fill_between(np.append(xs1,xs2),nnn/num,(nnn+1)/num, color='k' if not nnn%2 else '0.9')
        
    xs1 = np.linspace(360-width/2,360, num=20)*np.pi/180
    xs2 = np.linspace(0, width/2, num=20)*np.pi/180
    ax3.fill_between(xs1,0.95,1, color='k')
    ax3.fill_between(xs2,0.95,1, color='k')
    
    xs1 = np.linspace(360-width/2,362-width/2, num=20)*np.pi/180
    xs2 = np.linspace(width/2-2, width/2, num=20)*np.pi/180
    ax3.fill_between(xs1,0,1, color='k')
    ax3.fill_between(xs2,0,1, color='k')
    
        
    plt.savefig('%s/ellipses.png' % (savepath),
                    bbox_inches='tight', dpi=500)
    plt.savefig('%s/ellipses.pdf' % (savepath),
                    bbox_inches='tight', dpi=500)
    plt.show()
    plt.close()
    
