# get paths to code an data for this machine
import path_register
pr = path_register.PathRegister()
datapath = pr.datapath
gitpath = pr.gitpath
plotpath = pr.plotpath
uvispath = pr.uvispath

# import some custom code
import sys
sys.path.insert(0, '%s/cassinipy' % gitpath)
sys.path.insert(0, '%s/cubehelix' % gitpath)

import cassinipy
import cubehelix
from datetime import datetime
import get_image
import matplotlib.colors as mcolors
import matplotlib.gridspec as gridspec
import matplotlib.patheffects as PathEffects
import matplotlib.pyplot as plt
import numpy as np
import os
import time_conversions as tc
import uvisdb

myblue='royalblue'
myred='crimson'
cmap_UV = cubehelix.cmap(reverse=False, start=0.4, rot=-0.5, gamma=1.5)

udb = uvisdb.UVISDB(update=False)
imglist = udb.currentDF
cd = cassinipy.CassiniData(datapath)

samplefile = r'E:\data\UVIS\PDS_UVIS\PROJECTIONS_RES2_ISS\COUVIS_0024\2008_239T03_37_51.fits'
fmt = '%Y_%jT%H_%M_%S'
thisdt = datetime.strptime(samplefile.split('\\')[-1].split('.')[0], fmt)
thiset = tc.datetime2et(thisdt)
thisind = (imglist['ET_START']-thiset).abs().argmin()

savepath = '{}/uvis_ppo/boundary_sample'.format(gitpath)
if not os.path.exists(savepath):
    os.makedirs(savepath)
    
    
#%%

fig = plt.figure()
fig.set_size_inches(17,6)
gs = gridspec.GridSpec(1,4, width_ratios=(1,1,1,0.05), wspace=0.1)

data, _, _ = get_image.getRedUVIS(samplefile)
data[data<0.1] = 0.1
thisimg = imglist.loc[306]
fmt = '%Y-%m-%d (DOY %j), %H:%M:%S'
title = '{}\n{}$\,$s, {}'.format(datetime.strftime(thisdt, fmt), thisimg['EXP'], thisimg['HEMISPHERE'])

for panel in range(0,3):
    ax = plt.subplot(gs[0,panel], projection='polar')
    
    # plot
    KR_MIN=0.5
    KR_MAX=30
    
    lonbins = np.linspace(0, 2*np.pi, num=np.shape(data)[0]+1)
    colatbins = np.linspace(0, 30, num=np.shape(data)[1]+1)
    quad = ax.pcolormesh(lonbins, colatbins, data.T, cmap=cmap_UV)
    quad.set_clim(0, KR_MAX)
    quad.set_norm(mcolors.LogNorm(KR_MIN,KR_MAX))
    ax.set_facecolor('gray')
    
    # plot colorbar
    if not panel:
        cbar = plt.colorbar(quad, cax=plt.subplot(gs[0,-1]), extend='both')
        cbar.set_label('Intensity (kR)', labelpad=10, fontsize=11, rotation=270)
        cbar.set_ticks(np.append(np.append(np.arange(KR_MIN,1,0.1),
                                           np.arange(1,10,1)),
                                 np.arange(10,KR_MAX+1,10)))
        cbar.ax.tick_params(labelsize=10)
    
    ticks = [0,1/2*np.pi,np.pi,3/2*np.pi,2*np.pi]
    ticklabels = ['00','06','12','18']
    for iii in range(len(ticklabels)):
        txt = ax.text(ticks[iii], 27, ticklabels[iii], color='w', fontsize=14, fontweight='bold',
                      ha='center',va='center')
        txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='k')])
    ax.set_xticks(ticks)
    ax.set_xticklabels([])
    ax.set_yticks([10,20,30])
    ax.set_yticklabels([])
    ax.grid('on', color='0.8', linewidth=1)
    ax.set_theta_zero_location("N")
    ax.set_rmax(30)
    ax.set_rmin(0)
    ax.set_title(title)
    
    if panel==0:
        # PPO phase angle
        ppo_n = thisimg['PPO_PHASE_N']
        ppo_s = thisimg['PPO_PHASE_S']
        if np.isfinite(ppo_n):
            ax.plot([ppo_n,ppo_n], [0,30], color=myred, lw=2, zorder=5)
            ax.plot([ppo_n,ppo_n], [0,30], color='w', lw=3, zorder=3)
            txt = ax.text(ppo_n+0.12, 27, 'N', color='w', fontsize=14, fontweight='bold',
                          ha='center',va='center')
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground=myred)])
        if np.isfinite(ppo_s):
            ax.plot([ppo_s,ppo_s], [0,30], color=myblue, lw=2, zorder=5)
            ax.plot([ppo_s,ppo_s], [0,30], color='w', lw=3, zorder=3)
            txt = ax.text(ppo_s+0.12, 27, 'S', color='w', fontsize=14, fontweight='bold',
                          ha='center',va='center')
            txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground=myblue)])
        # Cassini orbit
        delta = 3600*24*1
        _, colat_n, colat_s, loct = cd.get_ionfootp(np.linspace(thiset-delta, thiset+delta, num=200))
        ax.plot(loct/12*np.pi, colat_n if thisimg['HEMISPHERE']=='North' else colat_s, c='w', ls='-', lw=1.5)
        # Cassini footprint
        _, colat_n, colat_s, loct = cd.get_ionfootp([thiset])
        ax.scatter(loct/12*np.pi, colat_n if thisimg['HEMISPHERE']=='North' else colat_s,
                   color='k', edgecolor='gold', marker='D', lw=2, zorder=5)
        # Cassini location print
        _, rs, lat, loct = cd.get_locations([thiset], refframe='KRTP')
        ax.text(0, 0, '{0:.2f} Rs\n{1:.2f}$^\circ$ {2}\n{3:.2f} LT'.format(
                        rs, np.abs(lat), thisimg['HEMISPHERE'][0], loct),
                transform=ax.transAxes)
    else:
        lonbins = np.linspace(0,2*np.pi,num=np.shape(thisimg['OPENCV_POLEW'])[-1]+1)
        loncenters = lonbins[:-1] + np.diff(lonbins)/2
        labels = ['OPENCV_CENTERS',
                  'OPENCV_POLEW',
                  'OPENCV_EQW']
        colors = ['k', myred, myblue]
        for iii in range(3 if panel==1 else 1):
            latval = thisimg[labels[iii]]
            ax.scatter(loncenters, latval,
                       marker='o',s=12, color='w',
                       edgecolors=colors[iii], linewidths=1.5)
        # draw ellipse/circle
        if panel==2:
            x, y, w, h, p, c = [thisimg['CIRCLE_X'],
                                thisimg['CIRCLE_Y'],
                                thisimg['CIRCLE_R'],
                                thisimg['CIRCLE_R'],
                                0, 'gold']   
            phispace = np.linspace(0,2*np.pi,num=500)
            coords = np.zeros((len(phispace), 2))
            for iii in range(len(phispace)):
                phi_el = phispace[iii] - p
                r_el = w*h/np.sqrt((h*np.cos(phi_el))**2+(w*np.sin(phi_el))**2)
                tmpx = x + r_el*np.cos(phispace[iii])
                tmpy = y + r_el*np.sin(phispace[iii])
                r = np.sqrt(tmpx**2 + tmpy**2)
                phi = np.arctan2(tmpy, tmpx)
                coords[iii,:] = [phi,r]
            ax.plot(coords[:,0], coords[:,1], c=c, lw=2.5)
            ax.plot(coords[:,0], coords[:,1], 'k:', lw=1, zorder=5)
            r_c = np.sqrt(x**2+y**2)
            p_c = np.arctan2(y,x)
            ax.scatter(p_c, r_c, marker='+', s=100, color=c,
                       edgecolor='none', linewidth=0.8, zorder=5)
    txt = ax.text(0, 1, '({})'.format('abcd'[panel]),
                  transform=ax.transAxes, ha='center', va='center',
                  color='k', fontweight='bold', fontsize=20)
    txt.set_path_effects([PathEffects.withStroke(linewidth=4, foreground='w')])
plt.savefig('{}/boundary_sample.png'.format(savepath), bbox_inches='tight', dpi=400)
plt.savefig('{}/boundary_sample.pdf'.format(savepath), bbox_inches='tight')

